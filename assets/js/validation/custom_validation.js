var char_space_regex   =  /^[a-zA-Z ]*$/;
var char_regex   =  /^[a-zA-Z]*$/;
var digit_regex   =  /^[0-9]*$/;
var digit_decimal_regex   =  /^[0-9.]*$/;
var emailRegex  = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;