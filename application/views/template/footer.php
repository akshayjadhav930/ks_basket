					<!-- Footer -->
					<footer class="dt-footer">
						Copyright Club SC © <?php echo date('Y') ?>
					</footer>
					<!-- /footer -->
				</div>
				<!-- /site content wrapper -->  
			   </main>
            </div>
        </div>
        <!-- masonry script -->
        <script src="<?php echo site_url() ?>assets/plugins/masonry-layout/js/masonry.pkgd.min.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/sweetalert2/js/sweetalert2.js"></script>
        <script src="<?php echo site_url() ?>assets/js/default/functions.js"></script>
        <script src="<?php echo site_url() ?>assets/js/default/customizer.js"></script>

        <script src="<?php echo site_url() ?>assets/js/default/script.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/amcharts.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/light.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/serial.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/pie.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/gauge.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/animate.min.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/amcharts3/js/responsive.min.js"></script>
        <script src="<?php echo site_url() ?>assets/js/global/charts/page-amcharts.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/datatables.net/js/jquery.dataTables.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
        <script src="<?php echo site_url() ?>assets/js/global/data-table.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/moment/js/locales.min.js"></script>
        <script src="<?php echo site_url() ?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <script src="<?php echo site_url() ?>assets/js/global/datetime-pickers.js"></script>
        <script src="<?php echo site_url(); ?>assets/js/toastr.min.js"></script>
		<script src="<?php echo site_url(); ?>assets/plugins/sweetalert2/js/sweetalert2.js"></script>
		<script src="<?php echo site_url(); ?>assets/js/validation/jquery.validate.js"></script>
        <script src="<?php echo site_url(); ?>/assets/js/global/color-pickers.js"></script>
		
		
    </body>
</html>
