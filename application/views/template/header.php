<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta tags -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Club SC" />
    <meta name="keywords" content="Electric Bike Showroom" />
    <!-- /meta tags -->
    <title>Club SC</title>

    <!-- Site favicon -->
    <link rel="shortcut icon" href="<?php echo site_url() ?>assets/images/favicon.ico" type="image/x-icon" />
    <!-- /site favicon -->

    <!-- Font Icon Styles -->
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/fonts/noir-pro/styles.css" />
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/plugins/flag-icon-css/css/flag-icon.min.css" />
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/vendor/gaxon-icon/styles.css" />
    <!-- /font icon Styles -->
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/custom.css" />
    <!-- Perfect Scrollbar stylesheet -->
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" />
    <!-- /perfect scrollbar stylesheet -->

    <link rel="stylesheet" type="text/css" href="<?php echo site_url() ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/default/theme-semidark.min.css" />
    <link rel="stylesheet" href="<?php echo site_url() ?>assets/plugins/spectrum-colorpicker/css/spectrum.css" />

    <link href="<?php echo site_url(); ?>assets/css/toastr.css" rel="stylesheet" type="text/css">


    <script>
        var rtlEnable = "";
        var $mediaUrl = "";
        var $baseUrl = "../";
        var current_path = window.location.href.split("/").pop();
        if (current_path == "") {
            current_path = "index.html";
        }
    </script>

    <script src="<?php echo site_url() ?>assets/plugins/jquery/js/jquery.min.js"></script>
    <script src="<?php echo site_url() ?>assets/plugins/moment/js/moment.min.js"></script>
    <script src="<?php echo site_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Perfect Scrollbar jQuery -->
    <script src="<?php echo site_url() ?>assets/plugins/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/plugins/chart.js/js/Chart.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/js/validation/custom_validation.js"></script>
    <script src="<?php echo site_url(); ?>assets/plugins/spectrum-colorpicker/js/spectrum.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@emretulek/jbvalidator"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <!-- /perfect scrollbar jQuery -->
</head>

<body class="dt-layout--default dt-sidebar--fixed dt-header--fixed">
    <!-- Loader -->
    <div class="dt-loader-container">
        <div class="dt-loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
            </svg>
        </div>
    </div>
    <!-- /loader -->
    <!-- Root -->
    <div class="dt-root">
        <div class="dt-root__inner">
            <!-- Header -->
            <header class="dt-header">
                <!-- Header container -->
                <div class="dt-header__container">
                    <!-- Brand -->
                    <div class="dt-brand">
                        <!-- Brand tool -->
                        <div class="dt-brand__tool" data-toggle="main-sidebar">
                            <div class="hamburger-inner"></div>
                        </div>
                        <!-- /brand tool -->

                        <!-- Brand logo -->
                        <span class="dt-brand__logo">
                            <a class="dt-brand__logo-link" href="<?php echo site_url() ?>" style="color: #ffffff;font-size: 18px;">
                                CLUB SC
                            </a>
                        </span>
                        <!-- /brand logo -->
                    </div>
                    <!-- /brand -->

                    <!-- Header toolbar-->
                    <div class="dt-header__toolbar">


                        <!-- Header Menu Wrapper -->
                        <div class="dt-nav-wrapper">

                            <!-- Header Menu -->
                            <ul class="dt-nav">
                                <li class="dt-nav__item dropdown">
                                    <!-- Dropdown Link -->
                                    <a href="#" class="dt-nav__link dropdown-toggle no-arrow dt-avatar-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon icon-wall text-primary"></i>
                                        <!-- <img class="dt-avatar size-30" src="https://via.placeholder.com/150x150" alt="Admin" /> -->
                                        <span class="dt-avatar-info d-none d-sm-block">
                                            <span class="dt-avatar-name"><?php echo $this->session->userdata('username') ?></span>
                                        </span>
                                    </a>
                                    <!-- /dropdown link -->

                                    <!-- Dropdown Option -->
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <div class="dt-avatar-wrapper flex-nowrap p-6 mt-n2 bg-gradient-purple text-white rounded-top">
                                            <i class="icon icon-wall dt-icon-bg bg-primary text-primary"></i>
                                            <!-- <img class="dt-avatar" src="https://via.placeholder.com/150x150" alt="Domnic Harris" /> -->
                                            <span class="dt-avatar-info">
                                                <span class="dt-avatar-name" style="color:white"><?php echo $this->session->userdata('username') ?></span>
                                            </span>
                                        </div>
                                        <a class="dropdown-item" href="<?php echo site_url() ?>login/logout">
                                            <i class="icon icon-editors icon-fw mr-2 mr-sm-1"></i>Logout
                                        </a>
                                    </div>
                                    <!-- /dropdown option -->
                                </li>
                            </ul>
                            <!-- /header menu -->
                        </div>
                        <!-- Header Menu Wrapper -->
                    </div>
                    <!-- /header toolbar -->
                </div>
                <!-- /header container -->
            </header>
            <!-- /header -->
            <!-- Site Main -->
            <main class="dt-main">
                <?php include_once('sidebar.php') ?>