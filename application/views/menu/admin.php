<li class="dt-side-nav__item">
    <a href="<?php echo site_url().'home' ?>" class="dt-side-nav__link dt-side-nav__arrow">
        <span class="dt-side-nav__text">Dashboard</span>
    </a>
</li>
<li class="dt-side-nav__item">
    <a href="javascript:void(0)" class="dt-side-nav__link dt-side-nav__arrow">
        <span class="dt-side-nav__text">Masters</span>
    </a>
    <ul class="dt-side-nav__sub-menu">
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'package' ?>" class="dt-side-nav__link">
                <i class="icon icon-dollar-circle icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Package</span>
            </a>
        </li>
        <!-- <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'customers' ?>" class="dt-side-nav__link">
                <i class="icon icon-contacts-app icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Customers</span>
            </a>
        </li> -->
    </ul>
</li>

<li class="dt-side-nav__item">
    <a href="javascript:void(0)" class="dt-side-nav__link dt-side-nav__arrow">
        <span class="dt-side-nav__text">Transactions</span>
    </a>
    <ul class="dt-side-nav__sub-menu">
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'registration' ?>" class="dt-side-nav__link">
                <i class="icon icon-addnew icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Registration</span>
            </a>
        </li>
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'renewal' ?>" class="dt-side-nav__link">
                <i class="icon icon-send icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Renewal</span>
            </a>
        </li>
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'checkup' ?>" class="dt-side-nav__link">
                <i class="icon icon-icons icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Checkup</span>
            </a>
        </li>
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'dailytips' ?>" class="dt-side-nav__link">
                <i class="icon icon-jumbotron icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">Daily Tips</span>
            </a>
        </li>
    </ul>
</li>

<!-- <li class="dt-side-nav__item">
    <a href="javascript:void(0)" class="dt-side-nav__link dt-side-nav__arrow">
        <span class="dt-side-nav__text">Utility</span>
    </a>
    <ul class="dt-side-nav__sub-menu">
        <li class="dt-side-nav__item">
            <a href="<?php echo site_url().'user' ?>" class="dt-side-nav__link">
                <i class="icon icon-user icon-fw icon-lg"></i>
                <span class="dt-side-nav__text">User Master</span>
            </a>
        </li>
    </ul>
</li> -->