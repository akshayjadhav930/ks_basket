<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Update Customer</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>customer/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Customer List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="customerForm" method="POST" class="needs-validation" novalidate>
							<div class="dt-entry__header">
								<div class="dt-entry__heading">
									<h3 class="dt-entry__title">Customer Details</h3>
								</div>
							</div>
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($customerData->id_customer) ? $customerData->id_customer : 0 ?>">
								<div class="col-sm-4 mb-2">
									<label for="name">Customer Name<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="name" placeholder="Enter Customer Name" value="<?php echo isset($customerData->name) ? $customerData->name : '' ?>" required maxlength="25">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="address">Address<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="address" placeholder="Enter Address" value="<?php echo isset($customerData->address) ? $customerData->address : '' ?>" required>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="mobile_no">Mobile No.<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="mobile_no" placeholder="Enter Amount" value="<?php echo isset($customerData->mobile_no) ? $customerData->mobile_no : '' ?>" pattern="[0-9]{10}\d*" required maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="email">Email</label>
									<input type="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo isset($customerData->email) ? $customerData->email : '' ?>" maxlength="150">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="profession">Profession</label>
									<input type="text" class="form-control" id="profession" placeholder="Enter profession" value="<?php echo isset($customerData->profession) ? $customerData->profession : '' ?>" maxlength="100">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="work_hours">Work Hours</label>
									<input type="text" class="form-control" id="work_hours" placeholder="Enter Work Hours" value="<?php echo isset($customerData->work_hours) ? $customerData->work_hours : '' ?>" pattern="(\d*\.)?\d*" maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="um">DOB<span class="req_span">*</span></label>
									<div class="form-group">
										<div class="form-group">
											<div class="input-group date" id="dpDOB" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" id="dpDOBTxt" data-target="#dpDOB" value="<?php echo isset($customerData) ? dcf($customerData->dob) : '' ?>" />
												<div class="input-group-append" data-target="#dpDOB" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="icon icon-calendar"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="instagram_id">Instagram Id</label>
									<input type="text" class="form-control" id="instagram_id" placeholder="Enter Instagram Id" value="<?php echo isset($customerData->instagram_id) ? $customerData->instagram_id : '' ?>" maxlength="40">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="facebook_id">Facebook Id</label>
									<input type="text" class="form-control" id="facebook_id" placeholder="Enter Facebook Id" value="<?php echo isset($customerData->facebook_id) ? $customerData->facebook_id : '' ?>" maxlength="40">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="height">Height Feet/Inches</label>
									<input type="text" class="form-control" id="height" placeholder="Enter Height" value="<?php echo isset($customerData->height) ? $customerData->height : '' ?>" pattern="(\d*\.)?\d*" maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="age">Age<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="age" placeholder="Enter Age" value="<?php echo isset($customerData->age) ? round($customerData->age) : '0' ?>" pattern="[0-9]\d*" required maxlength="10">
								</div>
							</div>
							
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i><?php echo isset($customerData) ? "UPDATE CUSTOMER" : "ADD CUSTOMER" ?></button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			var calIcons = {
				time: 'icon icon-time',
				date: 'icon icon-calendar',
				up: 'icon icon-chevrolet-up',
				down: 'icon icon-chevrolet-down',
				previous: 'icon icon-chevrolet-left',
				next: 'icon icon-chevrolet-right',
				clear: 'icon icon-trash'
			};
			$('#dpDOB').datetimepicker({
				format: 'DD/MM/YYYY',
				icons: calIcons,
				autoClose: true
			});
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});
			var classCustomer = {
				init: function() {},
				addCustomer: function(data) {
					$.ajax({
						url: "<?php echo site_url('customer/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('customer') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			$("#customerForm").submit(function(e) {
				e.preventDefault();
				if ($('#customerForm').valid()) {

					$("#btnSubmit").prop("disabled", true);
					let data = {
						edit_id: $('#edit_id').val(),
						name: $('#name').val(),
						address: $('#address').val(),
						mobile_no: $('#mobile_no').val(),
						email: $('#email').val(),
						profession: $('#profession').val(),
						work_hours: $('#work_hours').val(),
						dob: $('#dpDOBTxt').val(),
						instagram_id: $('#instagram_id').val(),
						facebook_id: $('#facebook_id').val(),
						height: $('#height').val(),
						age: $('#age').val()
					}
					classCustomer.addCustomer(data);
				}
			});
		});
	</script>