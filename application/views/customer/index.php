<div class="dt-content-wrapper">
    <div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title font-weight-bold">Customers</h1>
		</div>
		<div class="row">
			<div class="col-md-12 mb-3" style="display: flex;justify-content: flex-end;">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="data-table"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:55%">Customer Name</th>
								<th style="width:20%">Mobile No.</th>
								<th style="width:10%">Age</th>
								<th style="width:10%">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach($customerList as $key=>$value){
						?>
							<tr class="gradeX">
								<td><?php echo $key+1 ?></td>
								<td class="text-uppercase"><?php echo $value->name ?></td>
								<td class="text-uppercase"><?php echo $value->mobile_no ?></td>
								<td class="text-uppercase"><?php echo round($value->age) ?></td>
								<td>
                                <a href="<?php echo site_url().'customer/edit/'.$value->id_customer ?>" type="button" class="btn btn-primary btn-xs"><i class="icon icon-editors icon-fw mr-2 mr-sm-1"></i>Edit</a>
								</td>
							</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<th>#</th>
								<th>Customer Name</th>
								<th>Mobile No.</th>
								<th>Age</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>