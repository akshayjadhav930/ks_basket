<div class="dt-content-wrapper">
    <div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Create/Update User</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>user/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>User List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="userForm" method="POST">
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($userData->id_user) ? $userData->id_user : 0 ?>">
								<div class="col-sm-6 mb-3">
									<label for="name"><strong>Name<span class="req_span">*</span></strong></label>
									<input type="text" class="form-control" id="name" placeholder="Full Name" value="<?php echo isset($userData->name) ? $userData->name : '' ?>">
									<span class="err_name error_span"></span>
								</div>
								<div class="col-sm-6 mb-3">
									<label for="username"><strong>Username<span class="req_span">*</span></strong></label>
									<input type="text" class="form-control" id="username" placeholder="Username" value="<?php echo isset($userData->username) ? $userData->username : '' ?>" >
									<span class="err_username error_span"></span>
								</div>
								<div class="col-sm-6 mb-5">
									<label for="password"><strong>Password<span class="req_span">*</span></strong></label>
									<input type="password" class="form-control" id="password" placeholder="Password" value="<?php echo isset($userData->password) ? decrypt($userData->password) : '' ?>" >
									<span class="err_password error_span"></span>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="password"><strong>Designation<span class="req_span">*</span></strong></label>
									<select class="form-control" id="designation" >
										<option value="1" <?php echo isset($userData) ? ($userData->designation == 1 ? 'selected' : '') : '' ?>>Production</option>
										<option value="2" <?php echo isset($userData) ? ($userData->designation == 2 ? 'selected' : '') : '' ?>>Dealer</option>
										<option value="3" <?php echo isset($userData) ? ($userData->designation == 3 ? 'selected' : '') : '' ?>>Service Center</option>
									</select>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="password"><strong>Active?</strong></label>
									<div class="dt-checkbox d-block mb-6">
										<input type="checkbox" id="is_active" <?php echo isset($userData) ? ($userData->is_active == 1 ? 'checked': '') : 'checked' ?>>
										<label class="dt-checkbox-content" for="checkbox-1"></label>
									</div>
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit font-weight-bold" type="submit" id="btnSubmit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i>ADD USER</button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset font-weight-bold" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
			var nameRegex   =  /^[a-zA-Z ]*$/;
			var usernameRegex = /^[a-zA-Z]*$/;
			var aadharRegex = /^[0-9]{12}$/;
			var emailRegex  = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

			var classUser  = {
				isFormValidated : function(data){
					if(data.name != '' && data.username != '' && data.password != ''){
						return true;
					}else{
						if(data.name == ''){
							toastr.error('Please enter name!','Error');
							return false;
						}else if(data.username == ''){
							toastr.error('Please enter username!','Error');
							return false;
						}else if(data.password == ''){
							toastr.error('Please enter password!','Error');
							return false;
						}else{
							return true;
						}
					}
				},
				addUser :  function(data){
					$.ajax({
						url: "<?php echo site_url('user/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if(response.error == 1){
								toastr.error(response.message,'Error');
								$("#btnSubmit").prop("disabled", false);
							}else{
								toastr.success(response.message,'Success');
								setTimeout(function(){ window.location.href = "<?php echo site_url('user') ?>"; }, 1000);
							}					
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}
			$("#userForm").submit(function(e){
				e.preventDefault();
				$("#btnSubmit").prop("disabled", true);
				let data = {
					edit_id     : $('#edit_id').val(),
					name        : $('#name').val(),
					username    : $('#username').val(),
					password    : $('#password').val(),
					designation : $('#designation').val(),
					is_active   : $('#is_active').prop("checked") == true ? 1 : 0
				}
				if(classUser.isFormValidated(data)){
					classUser.addUser(data);
				}
			});
		});
	</script>
