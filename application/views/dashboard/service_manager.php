<style>
    .slot-block {
        width: 100%;
        height: 100%;
        color: white;
        text-align: center;
        cursor: pointer;
    }

    .slot-block-success {
        background-color: green;
    }

    .slot-block-failed {
        background-color: red;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="dt-card">
            <table class="table  table-bordered  table-sm">
                <thead>
                    <tr>
                        <th></th>
                        <th colspan="16">APPOINTMENT (SR)</th>
                    </tr>
                    <tr>
                        <th>DAY</th>
                        <th colspan="4">P1</th>
                        <th colspan="4">P2</th>
                        <th colspan="4">P3</th>
                        <th colspan="4">P4</th>
                    </tr>
                    <tr>
                        <th>HOURS/ SLOT</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                        <th>2</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // pr($slots);
                    foreach ($slots as $key => $slot) {
                    ?>
                        <tr>
                            <td><?php echo $key ?></td>
                            <?php
                            foreach ($slot as $sl) {
                                $arr = array(0, 1, 2, 3);
                                foreach ($arr as $ar) {
                            ?>
                                    <td>
                                        <?php
                                        if ($sl['isAvailable'] == 0) {// slots not available by Service center
                                        ?>
                                            <div class="slot-block slot-block-failed">NA</div>
                                        <?php
                                        }else if(isset($sl['bookings'][$ar])){
                                        ?>
                                            <div class="slot-block slot-block-failed" data-toggle="tooltip" data-html="true" title="<?php echo $sl['bookings'][$ar]->primary_name ?>">BOK</div>
                                        <?php
                                        }else{
                                        ?>
                                            <div class="slot-block slot-block-success">Y</div>
                                        <?php
                                        }
                                        ?>
                                    </td>
                            <?php
                                }
                            }
                            ?>




                            <!-- <td>
                                <?php
                                if ($slot[0]['isAvailable'] == 1 && $slot[0]['booking_count'] < 1) {
                                ?>
                                    <div class="slot-block slot-block-success">Y</div>
                                <?php
                                } else if ($slot[0]['isAvailable'] == 1 && $slot[0]['booking_count'] > 1) {
                                ?>
                                    <div class="slot-block slot-block-failed" data-toggle="tooltip" data-html="true" title="<?php echo $slot[0]['bookings'][0]->primary_name ?>">N</div>
                                <?php
                                } else if ($slot[0]['isAvailable'] == 0) {
                                ?>
                                    <div class="slot-block slot-block-failed" data-toggle="tooltip" data-html="true" title="<?php echo $slot[0]['bookings'][0]->primary_name ?>">N</div>
                                <?php
                                }
                                ?>
                            </td>
                            <td><?php echo ($slot[0]['booking_count'] < 2) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[0]['booking_count'] < 3) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[0]['booking_count'] < 4) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>

                            <td><?php echo ($slot[1]['booking_count'] < 1) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[1]['booking_count'] < 2) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[1]['booking_count'] < 3) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[1]['booking_count'] < 4) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>

                            <td><?php echo ($slot[2]['booking_count'] < 1) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[2]['booking_count'] < 2) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[2]['booking_count'] < 3) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[2]['booking_count'] < 4) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>

                            <td><?php echo ($slot[3]['booking_count'] < 1) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[3]['booking_count'] < 2) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[3]['booking_count'] < 3) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td>
                            <td><?php echo ($slot[3]['booking_count'] < 4) ? '<div class="slot-block slot-block-success">Y</div>' : '<div class="slot-block slot-block-failed">N</div>' ?></td> -->
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>