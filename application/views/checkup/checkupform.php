<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Add/Update Checkup</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>checkup/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Checkup List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="checkupForm" method="POST" class="needs-validation" novalidate>
							<div class="dt-entry__header">
								<div class="dt-entry__heading">
									<h3 class="dt-entry__title">Checkup Details</h3>
								</div>
							</div>
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($checkupData->id_checkup) ? $checkupData->id_checkup : 0 ?>">
								<div class="col-sm-4 mb-2">
									<label for="um">Checkup Date<span class="req_span">*</span></label>
									<div class="form-group">
										<div class="form-group">
											<div class="input-group date" id="dpCheckupDate" data-target-input="nearest">
												<input type="text" <?php echo isset($checkupData) ? 'disabled' : ''  ?> class="form-control datetimepicker-input" id="dpCheckupDateTxt" data-target="#dpDOB" value="<?php echo isset($checkupData) ? dcf($checkupData->checkup_date) : date('d/m/Y') ?>" />
												<div class="input-group-append" data-target="#dpCheckupDate" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="icon icon-calendar"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="name">Customer Name<span class="req_span">*</span></label>
									<select class="form-control" id="sbCustomer" <?php echo isset($checkupData) ? 'disabled' : ''  ?>>
										<?php
										foreach ($customerList as $customer) {
										?>
											<option value="<?php echo $customer->id_customer ?>"><?php echo $customer->name ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="weight">Weight<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="weight" placeholder="Enter Weight" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->weight) ? $checkupData->weight : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="visceral_fat">Visceral Fat<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="visceral_fat" placeholder="Enter Visceral Fat	" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->visceral_fat) ? $checkupData->visceral_fat : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="tsf">TSF<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="tsf" placeholder="Enter TSF" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->tsf) ? $checkupData->tsf : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="total_fat_per">Total Fat %<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="total_fat_per" placeholder="Enter Fat %" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->total_fat_per) ? round($checkupData->total_fat_per) : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="metabolic_age">Metabolic Age<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="metabolic_age" placeholder="Enter Fat %" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->metabolic_age) ? round($checkupData->metabolic_age) : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="bmi">BMI<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="bmi" placeholder="Enter BMI" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->bmi) ? round($checkupData->bmi) : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="bmr">BMR<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="bmr" placeholder="Enter BMR" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->bmr) ? round($checkupData->bmr) : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-2 mb-2">
									<label for="muscles_per">Muscles(%)<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="muscles_per" placeholder="Enter BMI" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->muscles_per) ? round($checkupData->muscles_per) : '' ?>" pattern="(\d*\.)?\d*" required>
								</div>
								<div class="col-sm-8 mb-2">
									<label for="remark">Remark</label>
									<input type="text" class="form-control" id="remark" placeholder="Enter Remark" <?php echo isset($checkupData) ? 'disabled' : ''  ?> value="<?php echo isset($checkupData->remark) ? round($checkupData->remark) : '' ?>">
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<?php
								if(!isset($checkupData)){
								?>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i><?php echo isset($checkupData) ? "UPDATE CHECKUP" : "ADD CHECKUP" ?></button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
								<?php
								}
								?>
								
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			var calIcons = {
				time: 'icon icon-time',
				date: 'icon icon-calendar',
				up: 'icon icon-chevrolet-up',
				down: 'icon icon-chevrolet-down',
				previous: 'icon icon-chevrolet-left',
				next: 'icon icon-chevrolet-right',
				clear: 'icon icon-trash'
			};
			$('#dpCheckupDate').datetimepicker({
				format: 'DD/MM/YYYY',
				icons: calIcons,
				autoClose: true
			});
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});
			var classCheckup = {
				init: function() {},
				addCheckup: function(data) {
					$.ajax({
						url: "<?php echo site_url('checkup/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('checkup') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			$("#checkupForm").submit(function(e) {
				e.preventDefault();
				if ($('#checkupForm').valid()) {
					$("#btnSubmit").prop("disabled", true);
					let data = {
						edit_id: $('#edit_id').val(),
						id_customer: $('#sbCustomer').val(),
						datetime: $('#dpCheckupDateTxt').val(),
						weight: $('#weight').val(),
						visceral_fat: $('#visceral_fat').val(),
						tsf: $('#tsf').val(),
						total_fat_per: $('#total_fat_per').val(),
						metabolic_age: $('#metabolic_age').val(),
						bmi: $('#bmi').val(),
						bmr: $('#bmr').val(),
						muscles_per: $('#muscles_per').val(),
						remark: $('#remark').val()
					}
					classCheckup.addCheckup(data);
				}
			});
		});
	</script>