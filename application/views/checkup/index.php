<div class="dt-content-wrapper">
    <div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title font-weight-bold">Weekly Checkup</h1>
		</div>
		<div class="row">
			<div class="col-md-12 mb-3" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>checkup/add" type="button" class="btn btn-primary btn-sm font-weight-bold">Add Checkup</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="data-table"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:15%">Date</th>
								<th style="width:55%">Customer Name</th>
								<th style="width:20%">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach($checkupList as $key=>$value){
						?>
							<tr class="gradeX">
								<td><?php echo $key+1 ?></td>
								<td class="text-uppercase"><?php echo dcf($value->datetime) ?></td>
								<td class="text-uppercase"><?php echo $value->name ?></td>
								<td>
									<a href="<?php echo site_url().'checkup/edit/'.$value->id_checkup ?>" type="button" class="btn btn-primary btn-xs"><i class="icon icon-editors icon-fw mr-2 mr-sm-1"></i>Preview</a>
									<button type="button" class="btn btn-danger btn-xs" onclick="deleteCheckup(<?php echo $value->id_checkup; ?>)"><i class="icon icon-trash-filled icon-fw mr-2 mr-sm-1"></i>Delete</button>
								</td>
							</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Customer Name</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	


<script>
	function deleteCheckup(id_checkup){
		const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success mb-2',
            cancelButtonClass: 'btn btn-danger mr-2 mb-2',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Are your sure?',
            text: "Do you want to delete?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel it!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
					url: "<?php echo site_url('checkup/delete') ?>",
					type: 'POST',
					data: {'id_checkup': id_checkup},
					success: function(response) {
						response = JSON.parse(response);
						if(response.error == 1){
							toastr.error(response.message,'Error');
						}else{
							toastr.success(response.message,'Success');
							setTimeout(function(){ window.location.href = "<?php echo site_url('checkup/index') ?>"; }, 1000);
						}					
					},
					async: false,
					error: function(request, status, error) {
						console.log(request.responseText);
					}
				});
            } else if (
				result.dismiss === swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons(
                    'Cancel',
                    'Cancelled',
                    'error'
                )
            }
		});
		
		
	}
</script>
