<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-content-wrapper" style="width:100%">
			<div class="dt-content" style="padding:0px">
				<div class="row">
					<div class="col-sm-3">
						<div class="dt-card">
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-5 mr-7 mr-sm-0">
									<i class="icon icon-leads1 dt-icon-bg bg-primary text-primary"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo $totalReg ?></span>
									</div>
									<div class="h5 mb-2" style="font-size: 18px;">Total Registrations</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="dt-card">
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-5 mr-7 mr-sm-0">
									<i class="icon icon-revenue-new dt-icon-bg bg-primary text-primary"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo count($upcomingRenewals); ?></span>
									</div>
									<div class="h5 mb-2" style="font-size: 18px;">Upcomming Renewals <i id="iconUpRenew" class="icon icon-send text-primary" style="cursor:pointer"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="dt-card">
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-5 mr-7 mr-sm-0">
									<i class="icon icon-birthday-new dt-icon-bg bg-primary text-primary"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo $todaysBirthdays; ?></span>
									</div>
									<div class="h5 mb-2" style="font-size: 18px;">Todays Birthdays <i class="icon icon-send text-primary birthday" style="cursor:pointer"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="dt-card">
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-5 mr-7 mr-sm-0">
									<i class="icon icon-birthday-new dt-icon-bg bg-primary text-primary"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo count($upcomingBirthdays); ?></span>
									</div>
									<div class="h5 mb-2" style="font-size: 18px;">Upcomming Birthdays <i class="icon icon-send text-primary birthday" style="cursor:pointer"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>

	<div class="modal fade" id="renewal-modal" tabindex="-1" role="dialog" aria-labelledby="model-8" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="width: 700px;">
			<div class="modal-content">
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-hover mb-0" id="tblEmployees">
							<thead>
								<tr>
									<th scope="col" style="width:10%"><strong>#</strong></th>
									<th class="text-uppercase" scope="col" style="width:60%"><strong>Name</strong></th>
									<th class="text-uppercase" scope="col" style="width:20%"><strong>Mobile No.</strong></th>
									<th class="text-uppercase" scope="col" style="width:10%"><strong>Renewal Date</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$c = 0;
								foreach ($upcomingRenewals as $renew) {
									$c++;
								?>
									<tr>
										<td><?php echo $c; ?></td>
										<td><?php echo $renew['name']  ?></td>
										<td><?php echo $renew['mobile_no']  ?></td>
										<td><?php echo $renew['renewal_date']  ?></td>
									</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" id="cancelRenewalModal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="birthday-modal" tabindex="-1" role="dialog" aria-labelledby="model-8" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document" style="width: 700px;">
			<div class="modal-content">
				<div class="modal-body">
					<div class="table-responsive">
						<table class="table table-hover mb-0" id="tblEmployees">
							<thead>
								<tr>
									<th scope="col" style="width:10%"><strong>#</strong></th>
									<th class="text-uppercase" scope="col" style="width:60%"><strong>Name</strong></th>
									<th class="text-uppercase" scope="col" style="width:20%"><strong>Mobile No.</strong></th>
									<th class="text-uppercase" scope="col" style="width:10%"><strong>DOB</strong></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$c = 0;
								foreach ($upcomingBirthdays as $birthday) {
									$c++;
								?>
									<tr>
										<td><?php echo $c; ?></td>
										<td><?php echo $birthday['name']  ?></td>
										<td><?php echo $birthday['mobile_no']  ?></td>
										<td><?php echo $birthday['dob']  ?></td>
									</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" id="cancelBirthdayModal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#iconUpRenew').click(() => {
				$("#renewal-modal").modal("show");
			})

			$('#cancelRenewalModal').click(() => {
				$("#renewal-modal").modal("hide");
			})

			$('.birthday').click(() => {
				$("#birthday-modal").modal("show");
			})

			$('#cancelBirthdayModal').click(() => {
				$("#birthday-modal").modal("hide");
			})
		})
	</script>