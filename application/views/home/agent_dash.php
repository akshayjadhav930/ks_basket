<!-- Site Content Wrapper -->
<div class="dt-content-wrapper">

    <!-- Site Content -->
    <div class="dt-content">
        <!-- Page Header -->
		<div class="dt-page__header">
			<h1 class="dt-page__title  font-weight-bold">एजंट</h1>
		</div>
		<!-- /page header -->

		<!-- Grid -->
		<div class="row">
			<!-- Grid Item -->
			<div class="col-xl-12 col-12 order-xl-2">

				<!-- Grid -->
				<div class="row">

					<!-- Grid Item -->
					<div class="col-sm-3">
						<!-- Card -->
						<div class="dt-card">

							<!-- Card Body -->
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-7 mr-7 mr-sm-0">
									<i class="icon icon-orders dt-icon-bg bg-secondary text-secondary"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo $contractor_agreements; ?></span>
									</div>
									<div class="h3 mb-2">वाहनमालक व मुकादम करार</div>
								</div>
							</div>
							<!-- /card body -->

						</div>
						<!-- /card -->
					</div>
					<!-- /grid item -->

					<!-- Grid Item -->
					<div class="col-sm-3">
						<!-- Card -->
						<div class="dt-card">

							<!-- Card Body -->
							<div class="dt-card__body d-flex flex-sm-column">
								<div class="mb-sm-7 mr-7 mr-sm-0">
									<i class="icon icon-orders dt-icon-bg bg-orange text-orange"></i>
								</div>
								<div class="flex-1">
									<div class="d-flex align-items-center mb-2">
										<span class="h2 mb-0 font-weight-500 mr-2"><?php echo $employee_agreements; ?></span>
									</div>
									<div class="h3 mb-2">मुकादम व कोयता करार</div>
								</div>
							</div>
							<!-- /card body -->

						</div>
						<!-- /card -->
					</div>
					<!-- /grid item -->
				</div>
				<!-- /grid -->

			</div>
			<!-- /grid item -->

		</div>
		<!-- /grid -->  

		<div class="row">
			<div class="col-md-12">
				<!-- Card -->
				<div class="dt-card">

					<!-- Card Header -->
					<div class="dt-card__header">

						<!-- Card Heading -->
						<div class="dt-card__heading">
							<h3 class="dt-card__title font-weight-bold">करार</h3>
						</div>
						<!-- /card heading -->

					</div>
					<!-- /card header -->

					<!-- Card Body -->
					<div class="dt-card__body tabs-container">

						<!-- Tab Navigation -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active font-weight-bold" data-toggle="tab" href="#home" role="tab"
								aria-controls="home"
								aria-selected="true">वाहनमालक व मुकादम</a>
							</li>
							<li class="nav-item">
								<a class="nav-link font-weight-bold" data-toggle="tab" href="#profile" role="tab"
								aria-controls="profile"
								aria-selected="true">मुकादम व कोयता</a>
							</li>
						</ul>
						<!-- /tab navigation -->

						<!-- Tab Content -->
						<div class="tab-content">

							<!-- Tab Pane -->
							<div id="home" class="tab-pane active">
								<!-- Grid -->
								<div class="row">

									<!-- Grid Item -->
									<div class="col-xl-12">
										<!-- Tables -->
										<div class="table-responsive" style="padding:20px">

											<table id="data-table"
												class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="font-weight-bold" style="width:5%">#</th>
														<th class="font-weight-bold" style="width:30%">वाहन मालक</th>
														<th class="font-weight-bold" style="width:30%">मुकादम</th>
														<th class="font-weight-bold" style="width:20%">करार तारीख</th>
														<th class="font-weight-bold" style="width:20%">डाउनलोड/करार रद्द</th>
													</tr>
												</thead>
												<tbody>
												<?php
													foreach($contractorAgreementList as $key=>$value){
												?>
													<tr class="gradeX">
														<td><?php echo $key+1 ?></td>
														<td><?php echo $value->transporter_name ?></td>
														<td><?php echo $value->contractor_name ?></td>
														<td><?php echo date('d/m/Y h:i:sa',strtotime($value->agreement_created_on)) ?></td>
														<td>
															<?php 
																if($value->is_cancelled == 0){
															?>
															<button type="button" class="btn btn-primary btn-xs font-weight-bold" onclick="dowloadFile('<?php echo site_url().'assets/agreements/transporter_'.$value->agreement_id.'.pdf'; ?>','<?php echo 'transporter_'.$value->agreement_id.'.pdf' ?>' )">डाउनलोड</button>
															<button type="button" class="btn btn-danger btn-xs font-weight-bold" onclick="cancelAgreement(<?php echo $value->agreement_id; ?>)">करार रद्द करा</button>
															<?php
																}else{
															?>
															<span class="badge badge-danger text-uppercase mb-2 font-weight-bold">करार रद्द</span>
															<?php
																}
															?>
															
														</td>
													</tr>
												<?php
													}
												?>
												</tbody>
												<tfoot>
													<tr>
														<th>#</th>
														<th>वाहन मालक</th>
														<th>मुकादम</th>
														<th>करार तारीख</th>
														<th>डाउनलोड/करार रद्द</th>
													</tr>
												</tfoot>
											</table>

										</div>
										<!-- /tables -->

									</div>
									<!-- /grid item -->

								</div>
								<!-- /grid --> 
							</div>
							<!-- /tab pane-->

							<!-- Tab Pane -->
							<div id="profile" class="tab-pane">
								<!-- Grid -->
								<div class="row">

									<!-- Grid Item -->
									<div class="col-xl-12">
										<!-- Tables -->
										<div class="table-responsive" style="padding:20px">

											<table id="data-table1" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="font-weight-bold" style="width:5%">#</th>
														<th class="font-weight-bold" style="width:35%">मुकादम</th>
														<th class="font-weight-bold" style="width:13%">करार तारीख</th>
														<th class="font-weight-bold" style="width:12%">डाउनलोड/करार रद्द</th>
													</tr>
												</thead>
												<tbody>
												<?php
													foreach($empAgreementList as $key=>$value){
												?>
													<tr class="gradeX">
														<td><?php echo $key+1 ?></td>
														<td><?php echo $value->contractor_name ?></td>
														<td><?php echo date('d/m/Y h:i:sa',strtotime($value->created_at)) ?></td>
														<td>
															<?php 
																if($value->is_cancelled == 0){
															?>
															<button type="button" class="btn btn-primary btn-xs font-weight-bold" onclick="dowloadFile('<?php echo site_url().'assets/agreements/contractor_'.$value->agreement_no.'.pdf'; ?>','<?php echo 'transporter_'.$value->agreement_no.'.pdf' ?>' )">डाउनलोड</button>
															<button type="button" class="btn btn-danger btn-xs font-weight-bold" onclick="cancelAgreement(<?php echo $value->agreement_no; ?>)">करार रद्द करा</button>
															<?php
																}else{
															?>
															<span class="badge badge-danger text-uppercase mb-2 font-weight-bold">करार रद्द</span>
															<?php
																}
															?>
															
														</td>
													</tr>
												<?php
													}
												?>
												</tbody>
												<tfoot>
													<tr>
														<th>#</th>
														<th>मुकादम</th>
														<th>करार तारीख</th>
														<th>डाउनलोड/करार रद्द</th>
													</tr>
												</tfoot>
											</table>

										</div>
										<!-- /tables -->

									</div>
									<!-- /grid item -->

								</div>
								<!-- /grid --> 
							</div>
							<!-- /tab pane-->

						</div>
						<!-- /tab content -->

					</div>
					<!-- /card body -->

				</div>
				<!-- /card -->
			</div>
		</div>
	</div>
	
