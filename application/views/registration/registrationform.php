<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Create/Update Registration</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>registration/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Registration List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="registrationForm" method="POST" class="needs-validation" novalidate>
							<div class="dt-entry__header">
								<div class="dt-entry__heading">
									<h3 class="dt-entry__title">Customer Details</h3>
								</div>
							</div>
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($registrationData->id_registration) ? $registrationData->id_registration : 0 ?>">
								<div class="col-sm-4 mb-2">
									<label for="name">Customer Name<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="name" placeholder="Enter Customer Name" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->name) ? $registrationData->name : '' ?>" required maxlength="25">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="address">Address<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="address" placeholder="Enter Address" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->address) ? round($registrationData->address) : '' ?>" required>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="mobile_no">Mobile No.<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="mobile_no" placeholder="Enter Amount" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->mobile_no) ? $registrationData->mobile_no : '' ?>" pattern="[0-9]{10}\d*" required maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="email">Email</label>
									<input type="email" class="form-control" id="email" placeholder="Enter email" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->email) ? $registrationData->email : '' ?>" maxlength="150">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="profession">Profession</label>
									<input type="text" class="form-control" id="profession" placeholder="Enter profession" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->profession) ? $registrationData->profession : '' ?>" maxlength="100">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="work_hours">Work Hours</label>
									<input type="text" class="form-control" id="work_hours" placeholder="Enter Work Hours" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->work_hours) ? $registrationData->work_hours : '' ?>" pattern="(\d*\.)?\d*" maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="um">DOB<span class="req_span">*</span></label>
									<div class="form-group">
										<!-- <div class="input-group date" id="dpDOB" data-target-input="nearest">
											<input type="text" class="form-control datetimepicker-input" id="dpDOBTxt" data-target="#dpDOB" value="<?php echo isset($registrationData) ? dcf($registrationData->dob) : '' ?>" />
											<div class="input-group-append" data-target="#dpDOB" data-toggle="datetimepicker">
												<div class="input-group-text"><i class="icon icon-calendar"></i>
												</div>
											</div>
										</div> -->
										<div class="form-group">
											<div class="input-group date" id="dpDOB" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" id="dpDOBTxt" data-target="#dpDOB" value="<?php echo isset($salesData) ? dcf($salesData->date_of_delivery) : '' ?>" />
												<div class="input-group-append" data-target="#dpDOB" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="icon icon-calendar"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="instagram_id">Instagram Id</label>
									<input type="text" class="form-control" id="instagram_id" placeholder="Enter Instagram Id" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->instagram_id) ? $registrationData->instagram_id : '' ?>" maxlength="40">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="facebook_id">Facebook Id</label>
									<input type="text" class="form-control" id="facebook_id" placeholder="Enter Facebook Id" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->facebook_id) ? $registrationData->facebook_id : '' ?>" maxlength="40">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="work_hours">Work Hours</label>
									<input type="text" class="form-control" id="work_hours" placeholder="Enter Work Hours" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->work_hours) ? $registrationData->work_hours : '' ?>" pattern="[0-9]\d*" maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="height">Height(cm)</label>
									<input type="text" class="form-control" id="height" placeholder="Enter Height" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->height) ? $registrationData->height : '' ?>" pattern="(\d*\.)?\d*" maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="age">Age<span class="req_span">*</span></label>
									<input type="text" class="form-control" id="age" placeholder="Enter Age" <?php echo isset($registrationData) ? 'disabled' : ''  ?> value="<?php echo isset($registrationData->age) ? $registrationData->age : '0' ?>" pattern="[0-9]\d*" required maxlength="10">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="gender">Gender<span class="req_span">*</span></label>
									<select class="form-control" id="sbGender" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
								</div>
							</div>
							<div class="dt-entry__header mt-3">
								<div class="dt-entry__heading">
									<h3 class="dt-entry__title">Registration Details</h3>
								</div>
							</div>
							<div class="form-row">
								<div class="col-sm-4 mb-2">
									<label for="package">Package<span class="req_span">*</span></label>
									<select class="form-control" id="sbPackage" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<?php
										foreach ($packageList as $package) {
										?>
											<option value="<?php echo $package->id_package ?>"><?php echo $package->package_name ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="paymentMethod">Payment Method<span class="req_span">*</span></label>
									<select class="form-control" id="sbPaymentMethod" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<option value="Cash">Cash</option>
										<option value="Bank">Bank</option>
										<option value="UPI">UPI</option>
									</select>
								</div>
							</div>
							<div class="dt-entry__header mt-3">
								<div class="dt-entry__heading">
									<h3 class="dt-entry__title">Other Details</h3>
								</div>
							</div>
							<div class="form-row">
								<div class="col-sm-4 mb-2">
									<label for="Reach">Reach From<span class="req_span">*</span></label>
									<select class="form-control" id="sbReach" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<option value="Facebook" <?php echo isset( $package->reach) ? ($package->reach == 'Facebook' ? 'selected' : '') : '' ?>>Facebook</option>
										<option value="Instagram" <?php echo isset( $package->reach) ? ($package->reach == 'Instagram' ? 'selected' : '') : '' ?>>Instagram</option>
										<option value="Reference" <?php echo isset( $package->reach) ? ($package->reach == 'Reference' ? 'selected' : '') : '' ?>>Reference</option>
									</select>
								</div>
								<div class="col-sm-4 mb-2" id="refBy">
									<label for="reference">Referred By<span class="req_span">*</span></label>
									<select class="form-control" id="sbReferred" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
									</select>
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i><?php echo isset($registrationData) ? "UPDATE REGISTRATION" : "ADD REGISTRATION" ?></button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			const isShowRegistration = '<?php echo isset($registrationData) ? ($registrationData->sbReferred == 'reference' ? 1 : 0) : 0 ?>';	
			if(+isShowRegistration){
				$('#refBy').show();
			}else{
				$('#refBy').hide();
			}
			
			var calIcons = {
				time: 'icon icon-time',
				date: 'icon icon-calendar',
				up: 'icon icon-chevrolet-up',
				down: 'icon icon-chevrolet-down',
				previous: 'icon icon-chevrolet-left',
				next: 'icon icon-chevrolet-right',
				clear: 'icon icon-trash'
			};
			$('#dpDOB').datetimepicker({
				format: 'DD/MM/YYYY',
				icons: calIcons,
				autoClose: true
			});
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});
			var classRegistration = {
				init: function() {
					classRegistration.getCustomerList();
				},
				addRegistration: function(data) {
					$.ajax({
						url: "<?php echo site_url('registration/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('registration') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				},
				getCustomerList: function() {
					let selIdCustomer = "<?php echo isset($registrationData) ? $registrationData->sbReferred : 0 ?>";
					$.ajax({
						url: "<?php echo site_url('registration/getCustomerList') ?>",
						type: 'POST',
						data: {},
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
							} else {
								console.log(response);
								$('#sbReferred').empty();
								optionText = "Select Customer";
								optionValue = "";
								$('#sbReferred').append($('<option>').val(optionValue).text(optionText));
								Object.keys(response.data).forEach(key => {
									optionText = response.data[key].name;
									optionValue = response.data[key].id_customer;
									$('#sbReferred').append($('<option>').val(optionValue).text(optionText).prop('selected', selIdCustomer == response.data[key].id_customer));
								});
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			classRegistration.init();

			$('#sbReach').change(function() {
				const val = $('#sbReach').val();
				if(val.toLowerCase() === 'reference'){
					$('#refBy').show();
				}else{
					$('#refBy').hide();
				}
			})

			$("#registrationForm").submit(function(e) {
				e.preventDefault();
				if ($('#registrationForm').valid()) {

					$("#btnSubmit").prop("disabled", true);
					let data = {
						edit_id: $('#edit_id').val(),
						name: $('#name').val(),
						address: $('#address').val(),
						mobile_no: $('#mobile_no').val(),
						email: $('#email').val(),
						profession: $('#profession').val(),
						work_hours: $('#work_hours').val(),
						dob: $('#dpDOBTxt').val(),
						gender: $('#sbGender').val(),
						instagram_id: $('#instagram_id').val(),
						facebook_id: $('#facebook_id').val(),
						height: $('#height').val(),
						age: $('#age').val(),
						id_package: $('#sbPackage').val(),
						payment_method: $('#sbPaymentMethod').val(),
						reach_from: $('#sbReach').val(),
						referred_by: $('#sbReach').val().toLowerCase() == 'reference' ? $('#sbReferred').val() : 0
					}
					classRegistration.addRegistration(data);
				}
			});
		});
	</script>