<div class="dt-content-wrapper">
    <div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title font-weight-bold">Renewals</h1>
		</div>
		<div class="row">
			<div class="col-md-12 mb-3" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>renewal/add" type="button" class="btn btn-primary btn-sm font-weight-bold">New Renewal</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="data-table"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:15%">Date</th>
								<th style="width:20%">Customer Name</th>
								<th style="width:10%">Package</th>
								<th style="width:10%">Status</th>
								<th style="width:10%">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach($registrationList as $key=>$value){
						?>
							<tr class="gradeX">
								<td><?php echo $key+1 ?></td>
								<td class="text-uppercase"><?php echo dcf($value->reg_date) ?></td>
								<td class="text-uppercase"><?php echo $value->name ?></td>
								<td class="text-uppercase"><?php echo $value->package_name ?></td>
								<td class="text-uppercase">
									<span class="badge badge-pill badge-<?php echo $value->status == 'active' ? 'success' : 'danger' ?> mb-1 mr-1"><?php echo $value->status ?></span>
								</td>
								<td>
									<button type="button" class="btn btn-danger btn-xs" onclick="deleteRegistartion(<?php echo $value->id_registration; ?>, <?php echo $value->id_customer; ?>)"><i class="icon icon-trash-filled icon-fw mr-2 mr-sm-1"></i>Delete</button>
								</td>
							</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Customer Name</th>
								<th>Package</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	


<script>
	function deleteRegistartion(id_registration){
		debugger
		const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success mb-2',
            cancelButtonClass: 'btn btn-danger mr-2 mb-2',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Are your sure?',
            text: "Do you want to delete?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel it!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
					url: "<?php echo site_url('renewal/delete') ?>",
					type: 'POST',
					data: {'id_registration': id_registration, 'id_customer': 0},
					success: function(response) {
						response = JSON.parse(response);
						if(response.error == 1){
							toastr.error(response.message,'Error');
						}else{
							toastr.success(response.message,'Success');
							setTimeout(function(){ window.location.href = "<?php echo site_url('renewal/index') ?>"; }, 1000);
						}					
					},
					async: false,
					error: function(request, status, error) {
						console.log(request.responseText);
					}
				});
            } else if (
				result.dismiss === swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons(
                    'Cancel',
                    'Cancelled',
                    'error'
                )
            }
		});
		
		
	}
</script>
