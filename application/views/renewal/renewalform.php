<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Create/Update Renewal</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>renewal/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Renewal List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="renewalForm" method="POST" class="needs-validation" novalidate>
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($registrationData->id_registration) ? $registrationData->id_registration : 0 ?>">
								<div class="col-sm-4 mb-2">
									<label for="um">Renewal Date<span class="req_span">*</span></label>
									<div class="form-group">
										<div class="form-group">
											<div class="input-group date" id="dpRenewalDate" data-target-input="nearest">
												<input type="text" class="form-control datetimepicker-input" id="dpRenewalDateTxt" data-target="#dpRenewalDate" value="<?php echo isset($salesData) ? dcf($salesData->date_of_delivery) : '' ?>" />
												<div class="input-group-append" data-target="#dpRenewalDate" data-toggle="datetimepicker">
													<div class="input-group-text"><i class="icon icon-calendar"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="name">Customer Name<span class="req_span">*</span></label>
									<select class="form-control" id="sbCustomer">
										<?php
										foreach ($customerList as $customer) {
										?>
											<option value="<?php echo $customer->id_customer ?>"><?php echo $customer->name ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="package">Package<span class="req_span">*</span></label>
									<select class="form-control" id="sbPackage" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<?php
										foreach ($packageList as $package) {
										?>
											<option value="<?php echo $package->id_package ?>"><?php echo $package->package_name ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-4 mb-2">
									<label for="paymentMethod">Payment Method<span class="req_span">*</span></label>
									<select class="form-control" id="sbPaymentMethod" <?php echo isset($registrationData) ? 'disabled' : ''  ?>>
										<option value="Cash">Cash</option>
										<option value="Bank">Bank</option>
									</select>
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i><?php echo isset($registrationData) ? "UPDATE RENEWAL" : "ADD RENEWAL" ?></button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});

			var calIcons = {
				time: 'icon icon-time',
				date: 'icon icon-calendar',
				up: 'icon icon-chevrolet-up',
				down: 'icon icon-chevrolet-down',
				previous: 'icon icon-chevrolet-left',
				next: 'icon icon-chevrolet-right',
				clear: 'icon icon-trash'
			};
			$('#dpRenewalDate').datetimepicker({
				format: 'DD/MM/YYYY',
				icons: calIcons,
				autoClose: true
			});
			var classRegistration = {
				init: function() {},
				addRegistration: function(data) {
					$.ajax({
						url: "<?php echo site_url('renewal/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('renewal') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			$("#renewalForm").submit(function(e) {
				e.preventDefault();
				if ($('#renewalForm').valid()) {

					$("#btnSubmit").prop("disabled", true);
					let data = {
						edit_id: $('#edit_id').val(),
						renewal_date: $('#dpRenewalDate').val(),
						id_customer: $('#sbCustomer').val(),
						id_package: $('#sbPackage').val(),
						payment_method: $('#sbPaymentMethod').val()
					}
					classRegistration.addRegistration(data);
				}
			});
		});
	</script>