<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Club SC">
        <meta name="keywords" content="Club SC">
        <!-- /meta tags -->
        <title>Club SC</title>

        <!-- Site favicon -->
        <link rel="shortcut icon" href="<?php echo site_url()?>assets/images/favicon.ico" type="image/x-icon">
        <!-- /site favicon -->

        <!-- Font Icon Styles -->
        <link rel="stylesheet" href="<?php echo site_url()?>assets/fonts/noir-pro/styles.css">
        <link rel="stylesheet" href="<?php echo site_url()?>assets/plugins/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo site_url()?>assets/vendor/gaxon-icon/styles.css">
        <!-- /font icon Styles -->

        <!-- Perfect Scrollbar stylesheet -->
        <link rel="stylesheet" href="<?php echo site_url()?>assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
        <!-- /perfect scrollbar stylesheet -->
        <!-- <link rel="stylesheet" href="<?php echo site_url()?>assets/css/default/theme-semidark.min.css"> -->

        <script>
            var rtlEnable = '';
                var $mediaUrl = '';
                var $baseUrl = '../';
            var current_path = window.location.href.split('/').pop();
            if (current_path == '') {
                current_path = 'index.html';
            }
        </script>

        <script src="<?php echo site_url()?>assets/plugins/jquery/js/jquery.min.js"></script>
        <script src="<?php echo site_url()?>assets/plugins/moment/js/moment.min.js"></script>
        <script src="<?php echo site_url()?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Perfect Scrollbar jQuery -->
        <script src="<?php echo site_url()?>assets/plugins/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
        <link rel="stylesheet" href="<?php echo site_url() ?>assets/css/custom.css" />
        <!-- /perfect scrollbar jQuery -->
        <link href="<?php echo site_url(); ?>assets/css/toastr.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Loader -->
        <div class="dt-loader-container">
            <div class="dt-loader">
                <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                </svg>
            </div>
        </div>
        <div class="dt-root">
            <div class="dt-root__inner">
                <div class="dt-login--container">
                    <div class="dt-login__content-wrapper">
                        <div class="dt-login__bg-section">
                            <div class="dt-login__bg-content">
                                <h1 class="dt-login__title">Login</h1>
                            </div>
                            <div class="dt-login__logo">
                                <a class="dt-brand__logo-link" style="color:#ffffff;font-size:20px" href="#">
                                    <img style="width:150px" src="<?php echo site_url().'assets/images/logo.jpg' ?>" >
                                </a>
                            </div>

                        </div>
                        <div class="dt-login__content">
                            <div class="dt-login__content-inner">

                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control form-control-sm" id="username" aria-describedby="username" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" maxlength="20" class="form-control form-control-sm" id="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary text-uppercase" id="login_btn">Login</button>
                                </div>

                            </div>

                        </div>
                        <!-- /login content section -->

                    </div>
                    <!-- /login content -->
                </div>        
            </div>        
        </div>
        <!-- /root -->

        <!-- masonry script -->
        <script src="<?php echo site_url()?>assets/plugins/masonry-layout/js/masonry.pkgd.min.js"></script>
        <script src="<?php echo site_url()?>assets/plugins/sweetalert2/js/sweetalert2.js"></script>
        <script src="<?php echo site_url()?>assets/js/default/functions.js"></script>
        <script src="<?php echo site_url()?>assets/js/default/customizer.js"></script>

        <script src="<?php echo site_url()?>assets/js/default/script.js"></script>
        <script src="<?php echo site_url(); ?>assets/js/toastr.min.js"></script>
    </body>
</html>


<script>
    $(document).ready(function(){
        $('#login_btn').click(function(){
            if($('#username').val() != '' && $('#password').val() != ''){
                $("#login_btn").prop("disabled", true);
                let postParams = {
                    'username' : $('#username').val(),
					'password' : $('#password').val(),
					'season_id': $('#season').val(),
					'season'   : $("#season option:selected").html()
                };
                
                $.ajax({
                    url: 'login/checklogin',
                    type: 'POST',
                    data : postParams,
                    dataType: "json",	
                    success: function (response){
                        if(response.error == 1){
                            toastr.error(response.message,'Error');
                            $("#login_btn").prop("disabled", false);
                        }else{
                            window.location.href = "home";		
                        }			
                    },
                    async: false,
                    error: function (request, status, error) {
                        console.log("error"+request.responseText);
                    }
                });	
            }else{
                if($('#username').val() == ''){
                    toastr.error('Enter username!','Error');
                }
                if($('#password').val() == ''){
                    toastr.error('Enter password!','Error');
                }
            }
        });
    });
</script>
