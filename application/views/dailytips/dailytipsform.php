<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Create Daily Tips</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>dailytips/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Tips List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="tipsForm" method="POST" class="needs-validation" novalidate>
							<div class="form-row">
								<div class="col-sm-12 mb-2">
									<label for="name">Tips<span class="req_span">*</span></label>
									<textarea type="text" class="form-control" id="tip_text" placeholder="Enter Tips" required></textarea>
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i>ADD TIP</button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});
			var classTips = {
				init: function() {},
				addTip: function(data) {
					$.ajax({
						url: "<?php echo site_url('dailytips/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('dailytips') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			$("#tipsForm").submit(function(e) {
				e.preventDefault();
				if ($('#tipsForm').valid()) {

					$("#btnSubmit").prop("disabled", true);
					let data = {
						tip_text: $('#tip_text').val()
					}
					classTips.addTip(data);
				}
			});
		});
	</script>