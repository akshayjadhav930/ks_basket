<div class="dt-content-wrapper">
	<div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title">Create/Update Package</h1>
		</div>
		<div class="row">
			<div class="col-md-12" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>package/index" type="button" class="btn btn-cust-with-icon btn-primary btn-sm font-weight-bold"> <i class="icon icon-list icon-fw icon-lg"></i>Package List</a>
			</div>
		</div>
		<div class="row" style="margin-top:30px">
			<div class="col-md-12">
				<div class="dt-card">
					<div class="dt-card__body">
						<form id="packageForm" method="POST" class="needs-validation" novalidate>
							<div class="form-row">
								<input type="hidden" id="edit_id" value="<?php echo isset($packageData->id_package) ? $packageData->id_package : 0 ?>">
								<div class="col-sm-4 mb-2">
									<label for="package"><strong>Package Name<span class="req_span">*</span></strong></label>
									<input type="text" class="form-control" id="package_name" placeholder="Enter Package Name" value="<?php echo isset($packageData->package_name) ? $packageData->package_name : '' ?>" required maxlength="25">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="amount"><strong>Amount<span class="req_span">*</span></strong></label>
									<input type="text" class="form-control" id="amount" placeholder="Enter Amount" value="<?php echo isset($packageData->amount) ? round($packageData->amount) : '0' ?>" pattern="[1-9.]\d*" min="0" required maxlength="5">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="validity"><strong>Validity in Days<span class="req_span">*</span></strong></label>
									<input type="text" class="form-control" id="validity" placeholder="Enter Amount" value="<?php echo isset($packageData->validity) ? $packageData->validity : '0' ?>" pattern="[1-9]\d*" required maxlength="5">
								</div>
								<div class="col-sm-4 mb-2">
									<label for="remark"><strong>Remark</strong></label>
									<input type="text" class="form-control" id="remark" placeholder="Enter remark" value="<?php echo isset($packageData->remark) ? $packageData->remark : '' ?>" maxlength="150">
								</div>
							</div>
							<div class="custom-card-footer card-footer">
								<button class="btn btn-primary btn-cust-with-icon btn-custom-submit font-weight-bold" id="btnSubmit" type="submit"> <i class="icon icon-circle-add-o icon-fw icon-lg"></i><?php echo isset($packageData) ? 'UPDATE PACKAGE' : 'ADD PACKAGE' ?></button>
								<button class="btn btn-primary btn-cust-with-icon btn-custom-reset font-weight-bold" type="reset"> <i class="icon icon-circle-remove-o icon-fw icon-lg"></i>RESET</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			let validator = $('form.needs-validation').jbvalidator({
				errorMessage: true,
				successClass: true,
				language: 'dist/lang/en.json'
			});
			var classPackage = {
				init: function() {},
				addPackage: function(data) {
					$.ajax({
						url: "<?php echo site_url('package/save') ?>",
						type: 'POST',
						data: data,
						success: function(response) {
							response = JSON.parse(response);
							if (response.error == 1) {
								toastr.error(response.message, 'Error');
								$("#btnSubmit").prop("disabled", false);
							} else {
								toastr.success(response.message, 'Success');
								setTimeout(function() {
									window.location.href = "<?php echo site_url('package') ?>";
								}, 1000);
							}
						},
						async: false,
						error: function(request, status, error) {
							console.log(request.responseText);
						}
					});
				}
			}

			$("#packageForm").submit(function(e) {
				e.preventDefault();
				if ($('#packageForm').valid()) {

					$("#btnSubmit").prop("disabled", true);
					let data = {
						edit_id: $('#edit_id').val(),
						package_name: $('#package_name').val(),
						amount: $('#amount').val(),
						validity: $('#validity').val(),
						remark: $('#remark').val()
					}
					classPackage.addPackage(data);
				}
			});
		});
	</script>