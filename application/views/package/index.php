<div class="dt-content-wrapper">
    <div class="dt-content">
		<div class="dt-page__header">
			<h1 class="dt-page__title font-weight-bold">Packages</h1>
		</div>
		<div class="row">
			<div class="col-md-12 mb-3" style="display: flex;justify-content: flex-end;">
				<a href="<?php echo site_url() ?>package/add" type="button" class="btn btn-primary btn-sm font-weight-bold">New Package</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="data-table"
						class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:20%">Package Name</th>
								<th style="width:20%">Amount</th>
								<th style="width:20%">Validity</th>
								<th style="width:15%">Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach($packageList as $key=>$value){
						?>
							<tr class="gradeX">
								<td><?php echo $key+1 ?></td>
								<td class="text-uppercase"><?php echo $value->package_name ?></td>
								<td class="text-uppercase"><?php echo round($value->amount) ?></td>
								<td class="text-uppercase"><?php echo $value->validity ?> Days</td>
								<td>
									<a href="<?php echo site_url().'package/edit/'.$value->id_package ?>" type="button" class="btn btn-primary btn-xs"><i class="icon icon-editors icon-fw mr-2 mr-sm-1"></i>Edit</a>
									<button type="button" class="btn btn-danger btn-xs" onclick="deletePackage(<?php echo $value->id_package; ?>)"><i class="icon icon-trash-filled icon-fw mr-2 mr-sm-1"></i>Delete</button>
								</td>
							</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<th>#</th>
								<th>Package Name</th>
								<th>Amount</th>
								<th>Validity</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	


<script>
	function deletePackage(id_package){
		const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success mb-2',
            cancelButtonClass: 'btn btn-danger mr-2 mb-2',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Are your sure?',
            text: "Do you want to delete?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel it!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
					url: "<?php echo site_url('package/delete') ?>",
					type: 'POST',
					data: {'id_package' : id_package},
					success: function(response) {
						response = JSON.parse(response);
						if(response.error == 1){
							toastr.error(response.message,'Error');
						}else{
							toastr.success(response.message,'Success');
							setTimeout(function(){ window.location.href = "<?php echo site_url('package/index') ?>"; }, 1000);
						}					
					},
					async: false,
					error: function(request, status, error) {
						console.log(request.responseText);
					}
				});
            } else if (
				result.dismiss === swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons(
                    'Cancel',
                    'Cancelled',
                    'error'
                )
            }
		});
		
		
	}
</script>
