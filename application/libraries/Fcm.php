<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fcm
{
    function sendFCM($tokens,$message) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array (
                'to' => $tokens,
                'notification' => array (
                        "body" => $message,
                        "title" => "AIRONET DEVICE NOTIFICATION",
                        "icon" => "/assets/images/ic_stat_pingme_logo.png"
                )
        );
        $fields = json_encode ( $fields );
        $headers = array (
                'Authorization: key=' . FCM_KEY,
                'Content-Type: application/json'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ( $ch );
        echo $result;
        curl_close ( $ch );
    }
}
