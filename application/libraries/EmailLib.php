<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailLib
{
    public function __construct(){
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load(){
        // Include PHPMailer library files
        require_once APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php';
        
        $mail = new PHPMailer;
        return $mail;
    }
}
