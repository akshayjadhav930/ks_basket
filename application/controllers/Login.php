<?php
class Login extends CI_Controller  {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('basic_model');
	}

	public function index(){
		if($this->session->userdata(LOGIN_KEY) == 1){
			redirect('home');
		}
		$this->load->view('login/index');
	}

	public function checkLogin(){
        $result = array();
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array('username' => $username);
		$userData = $this->basic_model->get_where('users', '*', $where);
		if(!empty($userData)){
			$pass = decrypt($userData[0]->password);
			
			if($password == $pass){

				$sessionData = array(
					'id_user'   => $userData[0]->id_user,
					'username'   => $userData[0]->username,
					'designation'=> $userData[0]->designation,
					LOGIN_KEY => 1
				);
				$this->session->set_userdata($sessionData);
				$result['error'] = 0;
				$result['message'] = "Successfully Logged In!";
			}else{
				$result['error'] = 1;
				$result['message'] = "Please check username and password!";
			}
		}else{
			$result['error'] = 1;
			$result['message'] = "Username not exist!";
		}
        echo json_encode($result);
	}

	public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }
}
