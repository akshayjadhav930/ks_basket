<?php
class Checkup extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('checkup_model');
	}

	public function index(){
		$data['checkupList'] =  $this->checkup_model->getCheckupData();
		$this->load->view('template/header');
		$this->load->view('checkup/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$where = array('is_deleted' => 0);
		$data['customerList'] =  $this->basic_model->get_where('customer', '*', $where);
		$this->load->view('template/header');
		$this->load->view('checkup/checkupform', $data);
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('is_deleted' => 0);
		$data['customerList'] =  $this->basic_model->get_where('customer', '*', $where);
		$data['checkupData'] =  $this->checkup_model->getCheckupData($id)[0];
		$this->load->view('template/header');
		$this->load->view('checkup/checkupform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$data = array(
			'id_customer' => $this->input->post('id_customer'),
			'datetime' => dct($this->input->post('datetime')),
			'weight' => remove_dashes($this->input->post('weight')),
			'visceral_fat' => remove_dashes($this->input->post('visceral_fat')),
			'tsf' => $this->input->post('tsf'),
			'total_fat_per' => $this->input->post('total_fat_per'),
			'metabolic_age' => $this->input->post('metabolic_age'),
			'bmi' => $this->input->post('bmi'),
			'bmr' => $this->input->post('bmr'),
			'muscles_per' => $this->input->post('muscles_per'),
			'remark' => $this->input->post('remark'),
			'created_at' => date('Y-m-d')
		);
		try{
			if($edit_id > 0){#update
				$where = array('sn' => remove_dashes($this->input->post('sn')),'id_batch' => $this->input->post('id_batch'),'id_part' => $this->input->post('id_part'), 'id_generic !=' => $edit_id, 'is_deleted' => 0);
				$exist   = $this->basic_model->get_where('generic', '*', $where);
				if(!empty($exist)){
					$result['error'] = 1;
					$result['message'] = "Generic already exist!";
				}else{
					$where = array('id_generic' => $edit_id, 'is_deleted' => 0);
					$this->basic_model->update_where('generic', $data, $where);
					$result['error'] = 0;
					$result['message'] = "Generic updated successfully!";
				}				
			}else{#add
				$where = array('id_customer' => $data['id_customer'],'datetime' => $data['datetime'], 'is_deleted' => 0);
				$exist = $this->basic_model->get_where('checkup', '*', $where);
				if(empty($exist)){#not exist
					$this->basic_model->add_to_table('checkup', $data);
					$result['error'] = 0;
					$result['message'] = "Checkup added successfully!";
				}else{#exist
					$result['error'] = 1;
					$result['message'] = "Todays checkup for this user already taken!";
				}
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_checkup   = $this->input->post('id_checkup');
		try{
			$where = array('id_checkup' => $id_checkup);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('checkup', $data, $where);
			$result['error'] = 0;
			$result['message'] = "Checkup deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}
}
