<?php
class Home extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('basic_model');
		$this->load->model('api_model');
		$this->load->model('home_model');
	}

	public function index(){
		$data = array();
		$where = array('is_deleted' => 0, 'is_new' => 1);
		$data['totalReg'] = $this->basic_model->get_count('registration', 'id_registration', $where);
		$data['upcomingRenewals'] = $this->home_model->getUpcomingRenewals();
		$data['upcomingBirthdays'] = $this->home_model->getUpcomingBirthdays();
		$count = 0;
		foreach($data['upcomingBirthdays'] as $days){
			if($days['isToday'] == 1){
				$count++;
			}
		}
		$data['todaysBirthdays'] = $count;
		$this->load->view('template/header');
		$this->load->view('home/index', $data);
		$this->load->view('template/footer');
	}

	public function getAvailability($date)
	{
		$result = array();
		$query = "SELECT * FROM slot_availability WHERE date = '$date'";
		$availability = $this->basic_model->execute_query($query);
		if (!empty($availability)) {
			foreach ($availability as $avail) {
				$tmp = array(
					'status' => $avail->status,
					'day_slot' => $avail->day_slot
				);
				array_push($result, $tmp);
			}
			return $result;
		} else {
			return 0;
		}
	}

}
