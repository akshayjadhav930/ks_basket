<?php
class Customer extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
	}

	public function index(){
        $where = array('is_deleted' => 0);
		$data['customerList'] =  $this->basic_model->get_where('customer', '*', $where);
		$this->load->view('template/header');
		$this->load->view('customer/index', $data);
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('is_deleted' => 0, 'id_customer' => $id);
		$data['customerData'] =  $this->basic_model->get_where('customer', '*', $where)[0];
		$this->load->view('template/header');
		$this->load->view('customer/customerform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$customerData = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'mobile_no' => $this->input->post('mobile_no'),
			'email' => $this->input->post('email'),
			'profession' => $this->input->post('profession'),
			'dob' => dct($this->input->post('dob')),
			'work_hours' => $this->input->post('work_hours'),
			'instagram_id' => $this->input->post('instagram_id'),
			'facebook_id' => $this->input->post('facebook_id'),
			'height' => $this->input->post('height'),
			'age' => $this->input->post('age'),
			'created_at' => date('Y-m-d H:i:s')
		);

		try{
			if($edit_id > 0){#update	
                $where = array('id_customer' => $edit_id);		
                $this->basic_model->update_where('customer', $customerData, $where);
                $result['error'] = 0;
			    $result['message'] = "Customer updated successfully";	
			}else{#add
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}
}
