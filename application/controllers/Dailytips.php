<?php
class Dailytips extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
	}

	public function index(){
        $where = array('is_deleted' => 0);
		$data['tipsList'] =  $this->basic_model->get_where('daily_tips', '*', $where);
		$this->load->view('template/header');
		$this->load->view('dailytips/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$this->load->view('template/header');
		$this->load->view('dailytips/dailytipsform');
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('id_tip' => $id, 'is_deleted' => 0);
		$data['tipsData'] =  $this->basic_model->get_where('daily_tips', '*', $where)[0];
		$this->load->view('template/header');
		$this->load->view('dailytips/dailytipsform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$data = array(
			'tip_text' => $this->input->post('tip_text'),
            'created_at' => date('Y-m-d H:i:sa')
		);
		try{
			if($edit_id > 0){#update				
			}else{#add
				$this->basic_model->add_to_table('daily_tips', $data);
                $result['error'] = 0;
                $result['message'] = "Daily tips added successfully!";
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_tip   = $this->input->post('id_tip');
		try{
			$where = array('id_tip' => $id_tip);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('daily_tips', $data, $where);
			$result['error'] = 0;
			$result['message'] = "Daily Tips deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}
}
