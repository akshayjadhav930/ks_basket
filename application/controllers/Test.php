<?php
require_once './vendor/autoload.php';
class Test extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('api_model');
    }

    public function index()
    {
        $mpdf = new \Mpdf\Mpdf();
        $html = '
                <html>
                <head>
                <style>
                body {font-family: sans-serif;
                    font-size: 10pt;
                }
                p {	margin: 0pt; }
                table.items {
                    border: 0.1mm solid #000000;
                }
                td { vertical-align: top; }
                .items td {
                    border-left: 0.1mm solid #000000;
                    border-right: 0.1mm solid #000000;
                }
                table thead td { background-color: #EEEEEE;
                    text-align: center;
                    border: 0.1mm solid #000000;
                    font-variant: small-caps;
                }
                .items td.blanktotal {
                    background-color: #EEEEEE;
                    border: 0.1mm solid #000000;
                    background-color: #FFFFFF;
                    border: 0mm none #000000;
                    border-top: 0.1mm solid #000000;
                    border-right: 0.1mm solid #000000;
                }
                .items td.totals {
                    text-align: right;
                    border: 0.1mm solid #000000;
                }
                .items td.cost {
                    text-align: "." center;
                }
                </style>
                </head>
                <body>
                <h2>CLUB SC</h2>
                <div style="text-align: right;">Date: 13th November 2008</div>
                <table width="100%" style="font-family: serif;"><tr>
                <td width="45%" style=""><span style="font-size: 7pt; color: #555555; font-family: sans;">TO:</span><br /><br />345 Anotherstreet<br />Little Village<br />Their City<br />CB22 6SO</td>
                <td width="10%">&nbsp;</td>
                
                </tr></table>
                <br />
                <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
                <thead>
                <tr>
                <td width="10%">Sr. No.</td>
                <td width="70%">Description</td>
                <td width="20%">Amount</td>
                </tr>
                </thead>
                <tbody>
                <!-- ITEMS HERE -->
                <tr>
                <td align="center">1</td>
                <td align="center">Package 1 </td>
                <td align="right">7000</td>
                </tr>

                <!-- END ITEMS HERE -->
                <tr>
                
                <td class="totals" colspan="2">Subtotal:</td>
                <td class="totals cost">1825.60</td>
                </tr>

                </tbody>
                </table>
                <div style="text-align: center; font-style: italic;">Payment terms: payment due in 30 days</div>
                </body>
                </html>
                ';
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
}
