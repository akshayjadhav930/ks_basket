<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEAD');
defined('BASEPATH') or exit('No direct script access allowed');
require_once './vendor/autoload.php';
class Api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('basic_model');
		$this->load->model('api_model');
	}

	public function checkLogin(){
		$post_data        = 	file_get_contents("php://input");
		$parsed_data      = 	json_decode($post_data, true);
		try {
			$where = array('username' => $parsed_data['username'], 'password' => $parsed_data['password']);
			$data = $this->basic_model->get_where('users', '*', $where);
			if(!empty($data)){
				$result['error'] = 0;
				$result['user'] = $parsed_data['username'];
				$result['message'] = "Welcome!";
			}else{
				$result['error'] = 1;
				$result['message'] = "Invalid Credentials!";
			}
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function getDashboard(){
		$result = array();
		try {
			$where = array('is_deleted' => 0);
			$result['total_items'] = $this->basic_model->get_count('items', 'id_item', $where);
			$query = "SELECT SUM(total_amt) as t_amt FROM sales";
			$result['total_sale'] = $this->basic_model->execute_query($query)[0]->t_amt;
			$result['total_customers'] = $this->basic_model->get_count('sales', 'DISTINCT(whatsapp_mobile)', $where);
			$result['error'] = 0;
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function getItemwiseSale(){
		$result = array();
		try {
			$data = array();
			$where = array('is_deleted' => 0);
			$items = $this->basic_model->get_where('items', '*', $where);
			
			foreach($items as $item){
				$query = "
						SELECT
							SUM(sales_has_items.item_cost) as sale_amt
						FROM
							sales
						LEFT JOIN
							sales_has_items ON (sales_has_items.id_sale = sales.id_sale)
						WHERE
							sales.is_deleted = 0 AND sales_has_items.id_item = ".$item->id_item;
				$res = $this->basic_model->execute_query($query);
				$itemData = array(
					'id_item' => $item->id_item,
					'item_name' => $item->item_name,
					'item_code' => $item->item_code,
					'amt' => !empty($res) ? $res[0]->sale_amt : 0
				);
				array_push($data, $itemData);
			}
			$result['error'] = 0;
			$result['data'] = $data;
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}
	
	public function addItem(){
		date_default_timezone_set('Asia/Kolkata');
		$post_data        = 	file_get_contents("php://input");
		$parsed_data      = 	json_decode($post_data, true);
		$result = array();
		try {
			$item_name = $parsed_data['item_name'];
			$item_img = $parsed_data['item_img'];
			$item_code = $parsed_data['item_code'];
			$item_cost = $parsed_data['item_cost'];
			$item_desc = $parsed_data['item_desc'];

			$where = array('is_deleted' => 0, 'item_code' => $item_code);
			$result = $this->basic_model->get_where('items', '*', $where);
			if(!empty($result)){
				$result['error'] = 1;
				$result['message'] = 'Item code already exist!';
			}else{
				$params = array(
					'item_name' => $item_name,
					'item_img'  => upload_base64($item_img),
					'item_code' => $item_code,
					'item_cost' => $item_cost,
					'item_desc' => $item_desc,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $parsed_data['user']
				);
				
				$this->basic_model->add_to_table('items', $params);
				$result['error'] = 0;
				$result['message'] = 'Item added successfully!';
			}
			
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function deleteItem(){
		date_default_timezone_set('Asia/Kolkata');
		$post_data        = 	file_get_contents("php://input");
		$parsed_data      = 	json_decode($post_data, true);
		$result = array();
		try {
			$id_item = $parsed_data['id'];
			$where = array('id_item' => $id_item);
			$data = array('is_deleted' => 1, 'deleted_by' => $parsed_data['user']);
			$result = $this->basic_model->update_where('items', $data, $where);
			$result['error'] = 0;
			$result['message'] = 'Item deleted successfully!';
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}
	
	public function getItems(){
		$result = array();
		try {
			$where = array('is_deleted' => 0);
			$data = $this->basic_model->get_where('items', '*', $where, '', 'created_at', 'DESC');
			if (empty($data)) {
				$result['error'] = 1;
				$result['message'] = 'Items not found!';
			} else {
				$result['error'] = 0;
				$result['data'] = $data;
			}
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function addSale(){
		date_default_timezone_set('Asia/Kolkata');
		$post_data        = 	file_get_contents("php://input");
		$parsed_data      = 	json_decode($post_data, true);
		$result = array();
		try {
			$sale_items = $parsed_data['sale_items'];
			
			$params = array(
				'customer_name' => $parsed_data['customer_name'],
				'town' => $parsed_data['town'],
				'total_amt' => $parsed_data['totalAmt'],
				'whatsapp_mobile' => $parsed_data['whatsapp_no'],
				'payment_method' => $parsed_data['payment_method'],
				'created_by' => $parsed_data['user'],
				'created_at' => date('Y-m-d H:i:s')
			);
			$id_sale = $this->basic_model->add_to_table('sales', $params);
			foreach($sale_items as $item){
				$dt = array(
					'id_sale' => $id_sale,
					'id_item' => $item['id_item'],
					'item_cost' => $item['item_cost']
				);
				$this->basic_model->add_to_table('sales_has_items', $dt);

				$where = array('id_item' => $item['id_item']);
				$data = array('is_sold' => 1);
				$this->basic_model->update_where('items', $data, $where);
			}

			$sms = "Dear ". $parsed_data['customer_name'] ."\n\nThank You for shopping with *KS BASKETS*.\n\nYour Total Bill amount is : ₹".$parsed_data['totalAmt']."\n\nWe would like to know your valuable feedback about our products.\n\nDO U LIKED THE RANGE & VERITY ?\n\n*PLZ REPLY : YES / NO*\n\nI WILL BE IN TOUCH WITH YOU, WITH MORE NEW DESIGNER BASKETS.\n\n *THANK YOU : KARUNA SHAH, KS BASKETS* ";
			$this->sendWhatsappSMS($sms, $parsed_data['whatsapp_no']);
			$result['error'] = 0;
			$result['message'] = 'Sales added successfully!';			
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function sendWhatsappSMS($sms, $mobile, $img_url=''){
	    
	    //https://whatsbot.tech/api/send_sms?api_token=26b95f52-e597-41c3-9462-bd4815bf978f&mobile=919028881366&message=neeraj
		$message = $sms;
		$curl = curl_init();
		$token = '1bcfb5d3-3c2f-47cb-9808-22dc37307f59';
        $message = curl_escape($curl,$message);
        $apiURL = '';
        if($img_url == ''){
            $apiUrl = "https://whatsbot.tech/api/send_sms?api_token=".$token."&mobile=91". $mobile ."&message=".$message."&img_url=". $img_url;
        }else{
            $apiUrl = "https://whatsbot.tech/api/send_img?api_token=".$token."&mobile=91". $mobile ."&img_caption=".$message."&img_url=". $img_url;
        }
		
		curl_setopt($curl, CURLOPT_URL, $apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
		// pr($output);
        curl_close($curl);
	}

	public function getSales(){
		$result = array();
		try {
			$where = array('is_deleted' => 0);
			$data = $this->basic_model->get_where('sales', '*', $where, '', 'created_at', 'DESC');
			if (empty($data)) {
				$result['error'] = 1;
				$result['message'] = 'Items not found!';
			} else {
				foreach($data as $dt){
					$query = "
					SELECT 
						items.item_name,
						items.item_code,
						items.item_desc,
						sales_has_items.id_sale,
						sales_has_items.item_cost
					FROM 
						sales_has_items
					LEFT JOIN
						items ON (items.id_item = sales_has_items.id_item)
					WHERE
						sales_has_items.id_sale = ".$dt->id_sale;
					$tmp = $this->basic_model->execute_query($query);
					$dt->sale_items = $tmp;
				}
				$result['error'] = 0;
				$result['data'] = $data;
			}
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function deleteSale(){
		date_default_timezone_set('Asia/Kolkata');
		$post_data        = 	file_get_contents("php://input");
		$parsed_data      = 	json_decode($post_data, true);
		$result = array();
		try {
			$id_sale = $parsed_data['id'];
			$where = array('id_sale' => $id_sale);
			$data = array('is_deleted' => 1, 'deleted_by' => $parsed_data['user']);
			$result = $this->basic_model->update_where('sales', $data, $where);
			$soldItems = $this->basic_model->get_where('sales_has_items', '*', $where);
			foreach($soldItems as $item){
				$where = array('id_item' => $item->id_item);
				$dtlData = array('is_sold' => 0);
				$this->basic_model->update_where('items', $dtlData, $where);
			}
			$result['error'] = 0;
			$result['message'] = 'Sale deleted successfully!';
		} catch (Exception $e) {
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}
	
}
