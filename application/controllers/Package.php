<?php
class Package extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
	}

	public function index(){
        $where = array('is_deleted' => 0);
		$data['packageList'] =  $this->basic_model->get_where('packages', '*', $where);
		$this->load->view('template/header');
		$this->load->view('package/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$this->load->view('template/header');
		$this->load->view('package/packageform');
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('id_package' => $id, 'is_deleted' => 0);
		$data['packageData'] = $this->basic_model->get_where('packages', '*', $where)[0];
		$this->load->view('template/header');
		$this->load->view('package/packageform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$data = array(
			'package_name' => $this->input->post('package_name'),
			'amount' => $this->input->post('amount'),
			'validity' => $this->input->post('validity'),
			'remark' => $this->input->post('remark')
		);
		try{
			if($edit_id > 0){#update
				$where = array('package_name' => $this->input->post('package_name'), 'id_package !=' => $edit_id, 'is_deleted' => 0);
				$exist   = $this->basic_model->get_where('packages', '*', $where);
				if(!empty($exist)){
					$result['error'] = 1;
					$result['message'] = "Package already exist!";
				}else{
					$where = array('id_package' => $edit_id, 'is_deleted' => 0);
					$this->basic_model->update_where('packages', $data, $where);
					$result['error'] = 0;
					$result['message'] = "Package updated successfully!";
				}				
			}else{#add
				$where = array('package_name' => $this->input->post('package_name'), 'is_deleted' => 0);
				$exist = $this->basic_model->get_where('packages', '*', $where);
				if(empty($exist)){#not exist
					$data['created_at'] = date('Y-m-d H:i:s');
					$this->basic_model->add_to_table('packages', $data);
					$result['error'] = 0;
					$result['message'] = "package added successfully!";
				}else{#exist
					$result['error'] = 1;
					$result['message'] = "package already exist!";
				}
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_package   = $this->input->post('id_package');
		try{
			$where = array('id_package' => $id_package);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('packages', $data, $where);
			$result['error'] = 0;
			$result['message'] = "Package deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}
}
