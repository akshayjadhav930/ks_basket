<?php
class Registration extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('registration_model');
	}

	public function index(){
		$data['registrationList'] =  $this->registration_model->getRegistrationData();
		$this->load->view('template/header');
		$this->load->view('registration/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$where = array('is_deleted' => 0);
		$data['packageList'] =  $this->basic_model->get_where('packages', '*', $where);
		$this->load->view('template/header');
		$this->load->view('registration/registrationform', $data);
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('is_deleted' => 0);
		$data['packageList'] =  $this->basic_model->get_where('packages', '*', $where);
		$data['registrationData'] =  $this->generic_model->getRegistrationData($id)[0];
		$this->load->view('template/header');
		$this->load->view('registration/registrationform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$customerData = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'mobile_no' => $this->input->post('mobile_no'),
			'email' => $this->input->post('email'),
			'profession' => $this->input->post('profession'),
			'dob' => dct($this->input->post('dob')),
			'gender' => $this->input->post('gender'),
			'work_hours' => $this->input->post('work_hours'),
			'instagram_id' => $this->input->post('instagram_id'),
			'facebook_id' => $this->input->post('facebook_id'),
			'height' => $this->input->post('height'),
			'age' => $this->input->post('age'),
			'password' => rand(111111,999999),
			'created_at' => date('Y-m-d H:i:s')
		);

		$registrationData = array(
			'id_package' => $this->input->post('id_package'),
			'payment_method' => $this->input->post('payment_method'),
			'is_new' => 1,
			'reach_from' => $this->input->post('reach_from'),
			'referred_by' => $this->input->post('referred_by'),
			'datetime' => date('Y-m-d H:i:s'),
			'created_at' => date('Y-m-d H:i:s')
		);

		try{
			if($edit_id > 0){#update				
			}else{#add
				#check customer mobile no is exist
				$where = array('mobile_no' => $this->input->post('mobile_no'), 'is_deleted' => 0);
				$existData = $this->basic_model->get_where('customer', '*', $where);
				if(!empty($existData)){
					$result['error'] = 1;
					$result['message'] = "Customer mobile already registered!";
				}else{
					$id_customer = $this->basic_model->add_to_table('customer', $customerData);
					$where = array('id_package' => $this->input->post('id_package'), 'is_deleted' => 0);
					$packData = $this->basic_model->get_where('packages', '*', $where)[0];
					$registrationData['id_customer'] = $id_customer;
					$registrationData['validity'] = $packData->validity;
					$registrationData['amount'] = $packData->amount;
					$this->basic_model->add_to_table('registration', $registrationData);
					$this->sendUserDetails($customerData['mobile_no'], $customerData['password']);
					$result['error'] = 0;
					$result['message'] = "Registration Successful!";
				}
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}
	
	public function sendUserDetails($username, $password){
		$message = "Welcome to Healthylife. Your app username is ". $username . " and password is ". $password;
		$curl = curl_init();
        $message = curl_escape($curl,$message);
		$apiUrl = "https://whatsbot.tech/api/send_sms?api_token=d4006d5b-634c-4334-ba5d-ac2f499347e4&mobile=91". $username ."&message=".$message;
		curl_setopt($curl, CURLOPT_URL, $apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
	}

	public function getCustomerList(){
		$result = array();
		try{
			$where = array('is_deleted' => 0);
			$customerData = $this->basic_model->get_where('customer', '*', $where);
			$result['error'] = 0;
			$result['data'] = $customerData;
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_registration   = $this->input->post('id_registration');
		$id_customer   = $this->input->post('id_customer');
		try{
			$where = array('id_registration' => $id_registration);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('registration', $data, $where);

			$where = array('id_customer' => $id_customer);
			$this->basic_model->update_where('customer', $data, $where);
			$result['error'] = 0;
			$result['message'] = "Registration deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}
}
