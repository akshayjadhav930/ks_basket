<?php
class User extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('basic_model');
		$this->load->helper('password_helper');
	}

	public function index(){
		$where = array('is_deleted' => 0);
		$data['userList'] =  $this->basic_model->get_where('users', '*', $where);
		$this->load->view('template/header');
		$this->load->view('user/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$data['designation'] = $this->session->userdata('designation');
		$this->load->view('template/header');
		$this->load->view('user/userform');
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$data['designation'] = $this->session->userdata('designation');
		$where = array('is_deleted' => 0, 'id_user' => $id);
		$data['userData'] =  $this->basic_model->get_where('users', '*', $where)[0];
		$this->load->view('template/header');
		$this->load->view('user/userform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => encrypt($this->input->post('password')),
			'designation' => $this->input->post('designation'),
			'is_active' => $this->input->post('is_active')
		);
		try{
			if($edit_id > 0){#update
				$where = array('username' => $this->input->post('username'), 'designation'=>$this->input->post('designation'), 'id_user !=' => $edit_id, 'is_deleted' => 0);
				$exist   = $this->basic_model->get_where('users', '*', $where);
				if(!empty($exist)){
					$result['error'] = 1;
					$result['message'] = "Username already exist!";
				}else{
					$where = array('id_user' => $edit_id, 'is_deleted' => 0);
					$this->basic_model->update_where('users', $data, $where);
					$result['error'] = 0;
					$result['message'] = "User updated successfully!";
				}				
			}else{#add
				$where = array('username'=>$this->input->post('username'), 'designation'=>$this->input->post('designation'), 'is_deleted' => 0);
				$exist = $this->basic_model->get_where('users', '*', $where);
				if(empty($exist)){#not exist
					$data['created_at'] = date('Y-m-d H:i:s');
					$id_user = $this->basic_model->add_to_table('users', $data);
					$result['error'] = 0;
					$result['message'] = "User added successfully!";
				}else{#exist
					$result['error'] = 1;
					$result['message'] = "Username already exist!";
				}
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_user   = $this->input->post('id_user');
		try{
			$where = array('id_user' => $id_user);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('users', $data, $where);
			$result['error'] = 0;
			$result['message'] = "User deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}

}
