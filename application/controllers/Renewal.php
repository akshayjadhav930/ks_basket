<?php
class Renewal extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('registration_model');
	}

	public function index(){
		$data['registrationList'] =  $this->registration_model->getRegistrationData(0,0);
		$this->load->view('template/header');
		$this->load->view('renewal/index', $data);
		$this->load->view('template/footer');
	}

	public function add(){
		$where = array('is_deleted' => 0);
		$data['packageList'] =  $this->basic_model->get_where('packages', '*', $where);
		$data['customerList'] =  $this->basic_model->get_where('customer', '*', $where);
		$this->load->view('template/header');
		$this->load->view('renewal/renewalform', $data);
		$this->load->view('template/footer');
	}

	public function edit($id = null){
		$where = array('is_deleted' => 0);
		$data['packageList'] =  $this->basic_model->get_where('packages', '*', $where);
		$data['customerList'] =  $this->basic_model->get_where('customer', '*', $where);
		$data['registrationData'] =  $this->generic_model->getRegistrationData($id, 0)[0];
		$this->load->view('template/header');
		$this->load->view('renewal/renewalform', $data);
		$this->load->view('template/footer');
	}

	public function save(){
		$result = array();
		$edit_id   = $this->input->post('edit_id');

		$registrationData = array(
			'id_package' => $this->input->post('id_package'),
			'payment_method' => $this->input->post('payment_method'),
			'is_new' => 0,
			'renewal_date' => dct($this->input->post('renewal_date')),
			'datetime' => date('Y-m-d H:i:s'),
			'created_at' => date('Y-m-d H:i:s')
		);

		try{
			if($edit_id > 0){#update				
			}else{#add
				#check customer mobile no is exist
				$where = array('mobile_no' => $this->input->post('mobile_no'), 'is_deleted' => 0);
				$existData = $this->basic_model->get_where('customer', '*', $where);
				if(!empty($existData)){
					$result['error'] = 1;
					$result['message'] = "Customer mobile already registered!";
				}else{
					$where = array('id_package' => $this->input->post('id_package'), 'is_deleted' => 0);
					$packData = $this->basic_model->get_where('packages', '*', $where)[0];
					$registrationData['id_customer'] = $this->input->post('id_customer');
					$registrationData['validity'] = $packData->validity;
					$registrationData['amount'] = $packData->amount;
					$this->basic_model->add_to_table('registration', $registrationData);
					$result['error'] = 0;
					$result['message'] = "Registration Successful!";
				}
			}
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server is unavailable, try later!";
		}
		echo json_encode($result);
	}

	public function delete(){
		$id_registration   = $this->input->post('id_registration');
		$id_customer   = $this->input->post('id_customer');
		try{
			$where = array('id_registration' => $id_registration);
			$data  = array('is_deleted' => 1);
			$this->basic_model->update_where('registration', $data, $where);

			$where = array('id_customer' => $id_customer);
			$this->basic_model->update_where('customer', $data, $where);
			$result['error'] = 0;
			$result['message'] = "Registration deleted successfully";
		}catch(Exception $e){
			$result['error'] = 1;
			$result['message'] = "Sorry! Server problem, please try later.";
		}
		echo json_encode($result);
	}
}
