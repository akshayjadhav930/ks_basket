<?php 
if(!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class MY_Controller extends CI_Controller{
    public function __construct(){
        header('Access-Control-Allow-Origin:*');
        header('Access-control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        header('Access-Control-Allow-Credentials: true');

        parent::__construct();
        $this->_is_secure();
    }

    private function _is_secure(){
        if($this->session->userdata(LOGIN_KEY) == 1){
            return true;
        }else{
            redirect('login');
        }
    }
}