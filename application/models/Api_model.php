<?php
class Api_model extends CI_Model {

    public function __construct() {
        $this->load->database();
	}

	public function checkSRStatus($id_customer){
		$query ="SELECT 
					service_request.*
				FROM 
					service_request
				WHERE 
					service_request.id_customer = $id_customer AND service_request.status NOT IN (4,5)";
		return $this->db->query($query)->result();
	}

	public function sendNotification($mobile, $notification, $token = ''){
		if($token == ''){
			$where = array('mobile_no' => $mobile);
			$token = $this->basic_model->get_one('fcm','token', $where);
		}
		if($token !== ''){
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
				'registration_ids' => array (
					$token
				),
				'data' => array (
						"message" => $notification
				)
			);
			//header includes Content type and api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key='.FCM_KEY
			);
						
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('FCM Send Error: ' . curl_error($ch));
			}
			curl_close($ch);
			return $result;
		}
	}	
}
