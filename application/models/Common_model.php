<?php
class Common_model extends CI_Model {

    public function __construct() {
        $this->load->database();
	}

    #list of generic parts that are not used in production
    public function getGenericAvail($name, $id_production = 0){
        $where = array('','is_deleted' => 0);
        $result = array();
        $query = "  SELECT
                        generic.*,
                        parts_master.*,
                        generic_batch.*
                    FROM
                        generic
                    LEFT JOIN
                        generic_batch ON (generic_batch.id_batch = generic.id_batch)
                    LEFT JOIN
                        parts_master ON (parts_master.id_part = generic.id_part)
                    WHERE
                        generic.is_deleted = 0 AND parts_master.part_name LIKE '$name'";
        $dataList = $this->db->query($query)->result();
        if(!empty($dataList)){
            $where = array('is_deleted' => 0, 'id_production !=' => $id_production);
            $productionList = $this->basic_model->get_where_array('production', '*', $where);
            foreach($dataList as $ctrl){
                $found = false;
                foreach($productionList as $prod){
                    $fields = array($prod['id_controller'],$prod['id_motor'],$prod['id_charger'],$prod['id_battery'],$prod['id_um']);
                    if(in_array($ctrl->id_generic, $fields)){
                        $found = true;
                    }
                }
                if(!$found){
                    array_push($result, $ctrl);
                }
            }
        }
        // pr($result);
        return $result;       
    }
}
