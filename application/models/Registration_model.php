<?php
class Registration_model extends CI_Model {

    public function __construct() {
        $this->load->database();
	}

    public function getRegistrationData($id_registration = 0, $isNew = 1){
        $where = " AND  registration.is_new = $isNew";
        if($id_registration > 0){
            $where .= " AND registration.id_registration = ".$id_registration;
        }
        $query ="SELECT 
					registration.*,
                    DATE(registration.datetime) as reg_date,
					customer.*,
                    packages.*
                FROM 
                    registration
				LEFT JOIN
                    customer ON (customer.id_customer = registration.id_customer)
                LEFT JOIN
                    packages ON (packages.id_package = registration.id_package)
				WHERE 
                    registration.is_deleted = 0 ".$where;
        $data = $this->db->query($query)->result();
        foreach($data as $dt){
            $today = date('Y-m-d');
            $lastDate = date('Y-m-d', strtotime($dt->reg_date. ' + '.$dt->validity));
            $dt->status = $today > $lastDate ? 'active' : 'not-active';
        }

        return $data;
    }
}
