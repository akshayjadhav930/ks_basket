<?php
class Checkup_model extends CI_Model {

    public function __construct() {
        $this->load->database();
	}

    public function getCheckupData($id_checkup = 0){
        $where = $id_checkup == 0 ? "" : "AND checkup.id_checkup = ".$id_checkup;
        $query ="SELECT 
					checkup.*,
                    DATE(checkup.datetime) as checkup_date,
                    customer.name
                FROM 
                    checkup
				LEFT JOIN
                    customer ON (customer.id_customer = checkup.id_customer)
                WHERE 
                checkup.is_deleted = 0 ".$where;
        return $this->db->query($query)->result();
    }
}
