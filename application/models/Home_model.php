<?php
class Home_model extends CI_Model {

    public function __construct() {
        $this->load->database();
	}

    public function getUpcomingRenewals(){
        $data = array();
        $where = array('is_deleted' => 0);
        $customers = $this->basic_model->get_where("customer", "*", $where);
        foreach($customers as $customer){
            $query = "
                        SELECT
                            registration.id_registration,
                            DATE(registration.datetime) as regdate,
                            packages.validity
                        FROM
                            registration
                        LEFT JOIN
                            packages ON (packages.id_package = registration.id_package)
                        WHERE
                            registration.id_customer = ".$customer->id_customer." AND registration.is_deleted = 0
                        ORDER BY
                            registration.id_registration DESC LIMIT 1";               
            $result = $this->basic_model->execute_query($query);
            if(!empty($result)){
                $days = diffDays($result[0]->regdate);
                if($days > ($result[0]->validity - 4) && $days <= $result[0]->validity){
                    $renewalDate = addDays($result[0]->regdate, $result[0]->validity);
                    $tmp = array(
                        'id_customer' => $customer->id_customer,
                        'name' => $customer->name,
                        'mobile_no' => $customer->mobile_no,
                        'renewal_date' => dcf($renewalDate),
                        'isToday' => $days = 0 ? 1 : 0
                    );
                    array_push($data, $tmp);
                }
            }
        }
        return $data;
    }

    public function getUpcomingBirthdays(){
        $data = array();
        $where = array('is_deleted' => 0);
        $customers = $this->basic_model->get_where("customer", "id_customer, mobile_no, name, dob, DATE_FORMAT(dob, '%m-%d') as dob2", $where);
        foreach($customers as $customer){
            $daysDiff = daysRemaining(date('Y').'-'.$customer->dob2);
            $daysDiff = $daysDiff + 1;
            if($daysDiff < 5 && $daysDiff >=0){
                $tmp = array(
                    'id_customer' => $customer->id_customer,
                    'name' => $customer->name,
                    'mobile_no' => $customer->mobile_no,
                    'dob' => dcf($customer->dob),
                    'isToday' => $daysDiff = 0 ? 1 : 0
                );
                array_push($data, $tmp);
            }
            
        }
        return $data;
    }
}
