<?php
class Basic_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

	#SELECT START
    public function get_where($table_name,$fields,$where,$limit='', $orderby_val = '',$order_by = ''){
        $this->db->select($fields);
        $this->db->where($where);
        if($limit != ''){
            $this->db->limit($limit);
        }
        if($order_by != '' && $orderby_val != ''){
            $this->db->order_by($orderby_val, $order_by);
        }
        $query = $this->db->get($table_name);
        return $query->result();
	}
	public function get_query_array($query){
		return $this->db->query($query)->result_array();
    }
    public function execute_query($query){
		return $this->db->query($query)->result();
	}
	
	public function get_where_array($table_name,$fields,$where){
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($table_name);
        return $query->result_array();
    }

    public function get_all($table_name,$value='',$orderby=''){
        $this->db->select('*');
		if($orderby != '' && $value != ''){
			$this->db->order_by($value,$orderby);
		}
		$query = $this->db->get($table_name);
		
        return $query->result();
	}
	
	public function get_all_array($table_name){
        $this->db->select('*');
        $query = $this->db->get($table_name);
        return $query->result_array();
	}

    public function add_to_table($table_name,$data){
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function get_row($table_name,$column_name,$where,$order_by){
        $this->db->select($column_name);
        $this->db->where($where);
        $this->db->limit(1);
        $this->db->order_by($order_by);
        $query  = $this->db->get($table_name);
        return $query->result();
    }

    public function get_one($table_name,$column_name,$where,$order_by = '', $orderby_val= ''){
        $this->db->select($column_name);
        $this->db->where($where);
        $this->db->limit(1);
		if($order_by != '' && $orderby_val != ''){
            $this->db->order_by($orderby_val, $order_by);
        }
        
        $query  = $this->db->get($table_name);
        $result = $query->result();
        if($result != null){
            return $result[0]->$column_name;
        }else{
            return 0;
        }
    }

    public function get_count($table_name,$colum_name,$where){
        $this->db->select('COUNT('.$colum_name.') as count');
        if($where!=""){
            $this->db->where($where);
        }
        $this->db->limit(1);
        $query  = $this->db->get($table_name);
        $result = $query->result();
        return $result[0]->count;

    }

    public function get_count_order_by($table_name,$colum_name,$where,$order_by){
        $this->db->select('COUNT('.$colum_name.') as count');
        if($where!=""){
            $this->db->where($where);
        }
        $this->db->order_by($order_by);
        $this->db->limit(1);
        
        $query  = $this->db->get($table_name);
        $result = $query->result();
        return $result[0]->count;
	}
	# SELECT END


	# INSERT START
	public function insert($table_name,$data){
		$this->db->insert($table_name,$data);
		return $this->db->insert_id();
	}
	# INSERT END

	# UPDATE START
	
	public function update_where($table_name,$data,$where){
		$this->db->where($where);
		$this->db->update($table_name, $data);
	}

	public function update_all($table_name,$data){
		$this->db->update($table_name, $data);
	}

	# UPDATE END

	#DELETE START

	public function delete_where($table_name,$where){
        $this->db->where($where);
        $this->db->delete($table_name);
	}

	#DELETE END


	#ENCRYPTION START
    public function encrypt($token){
        $cipher_method = 'aes-128-ctr';
        $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
        $enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
        $crypted_token = openssl_encrypt($token, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
        unset($token, $cipher_method, $enc_key, $enc_iv);

        return $crypted_token;
    }

    public function decrypt($token){
        list($crypted_token, $enc_iv) = explode("::", $token);
        $cipher_method = 'aes-128-ctr';
        $enc_key = openssl_digest(php_uname(), 'SHA256', TRUE);
        $token = openssl_decrypt($crypted_token, $cipher_method, $enc_key, 0, hex2bin($enc_iv));
        unset($crypted_token, $cipher_method, $enc_key, $enc_iv);

        return $token;
    }

	#ENCRYPTION END

    function send_email($path){
        $data = array();
        $name = 'akshay jadhav';
        $email = 'akshayjadhav930@gmail.com, sourabh.chavan499@gmail.com ';
        $this->load->library('EmailLib');
        
        // PHPMailer object
        $mail = $this->emaillib->load();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.ashvasoft.in'; 
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'contact@ashvasoft.in';
        $mail->Password = ';dMLWF82}@tU';
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = '587';                                    // TCP port to connect to
        
        $mail->setFrom('contact@ashvasoft.in', 'ASHVASOFT TECHNOLOGY');
        $mail->addAddress($email);   // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->addAttachment($path);
        $mail->Subject = "OTP VERIFICATION ";
        $mail->Body    = "Your Invoice";
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo "success";
        }
    }
}
