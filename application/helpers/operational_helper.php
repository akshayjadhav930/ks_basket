<?php

if (!function_exists('get_designation_name')) {
    function get_designation_name($id)
    {
        switch ($id) {
            case 1:
                return 'Production';
                break;
            case 2:
                return 'Dealer';
                break;
            case 3:
                return 'Service Center';
                break;
            default:
                return 'Invalid';
        }
    }
}

if (!function_exists('add_dashes')) {
    function add_dashes($str, $positions)
    {

        if ($positions == 0 || $positions == '') {
            return $str;
        }
        // $arr = str_split($positions,1);
        $arr = explode(",", $positions);
        // $str_length = count($arr);
        $tmp = '';
        $last_position = 0;

        for ($i = 0; $i < count($arr); $i++) {
            if ($i + 1 < count($arr)) {
                $tmp .= substr($str, $last_position, $arr[$i]) . '-';
            } else {
                $tmp .= substr($str, $last_position, $arr[$i]);
            }
            $last_position = $last_position + $arr[$i];
        }
        return $tmp;
    }
}

if (!function_exists('remove_dashes')) {
    function remove_dashes($str)
    {
        return str_replace("-", "", $str);
    }
}

if (!function_exists('getSlotTime')) {
    function getSlotTime($slot)
    {
        switch ($slot) {
            case 1:
                return '10 AM';
                break;
            case 2:
                return '12 PM';
                break;
            case 3:
                return '02 PM';
                break;
            case 4:
                return '04 PM';
                break;
            default:
                return 'All Day';
        }
    }
}

if (!function_exists('get_slot_no')) {
    function get_slot_no()
    {
        $firstSlot = strtotime(date('Y-m-d') . ' 10:00:00');
        $secondSlot = strtotime(date('Y-m-d') . ' 12:00:00');
        $thirdSlot = strtotime(date('Y-m-d') . ' 14:00:00');
        $fourthSlot = strtotime(date('Y-m-d') . ' 16:00:00');
        $fifthSlot = strtotime(date('Y-m-d') . ' 18:00:00');
        $currentDate = strtotime(date('Y-m-d H:i:s'));
        if ($firstSlot < $currentDate && $secondSlot >= $currentDate) {
            return 1;
        } else if ($secondSlot < $currentDate && $thirdSlot >= $currentDate) {
            return 2;
        } else if ($thirdSlot < $currentDate && $fourthSlot >= $currentDate) {
            return 3;
        } else if ($fourthSlot < $currentDate && $fifthSlot >= $currentDate) {
            return 4;
        } else {
            return 0;
        }
    }
}

if (!function_exists('get_avail_status')) {
    function get_avail_status($status)
    {
        switch ($status) {
            case 1:
                return 'Agent Not Available';
                break;
            case 2:
                return 'Rescheduled';
                break;
            default:
                return 0;
        }
    }
}

if (!function_exists('pr')) {
    function pr($data)
    {
        echo '<pre>';
        print_r($data);
        exit();
    }
}

if (!function_exists('dct')) {
    function dct($date)
    {
        $dt = explode('/', $date);
        $dt = $dt[2] . '-' . $dt[1] . '-' . $dt[0];
        $dt = date('Y-m-d', strtotime($dt));
        return $dt;
    }
}


if (!function_exists('dcf')) {
    function dcf($date)
    {
        $dt = date('d/m/Y', strtotime($date));
        return $dt;
    }
}

if (!function_exists('upload_base64')) {
    function upload_base64($img)
    {
        try {
            $path = "assets/uploads/";
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $file = $path . uniqid() . '.jpeg';
            $success = file_put_contents($file, $data);
        } catch (Exception $e) {
            print_r($e);
            exit();
        }

        return site_url() . $file;
    }
}

if (!function_exists('getMediaType')) {
    function getMediaType($type)
    {
        switch ($type) {
            case 1:
                return 'Image';
                break;
            case 2:
                return 'Video';
                break;
            case 3:
                return 'Audio';
                break;
            default:
                return 'Image';
        }
    }
}

if(!function_exists('diffHours')){
    function diffHours($dt){
        date_default_timezone_set("Asia/Calcutta");
        $now = new DateTime();
        $future_date = new DateTime($dt);
        $interval = $future_date->diff($now);
        return $interval->format("%i");
    }
}

if(!function_exists('diffDays')){
    function diffDays($dt){
        date_default_timezone_set("Asia/Calcutta");
        $now = new DateTime();
        $future_date = new DateTime($dt);
        $interval = $future_date->diff($now);
        return $interval->format("%d");
    }
}

if(!function_exists('addDays')){
    function addDays($dt, $days){
        date_default_timezone_set("Asia/Calcutta");
        return date('Y-m-d', strtotime($dt. ' + '.$days.' days'));
    }
}

if(!function_exists('daysRemaining')){
    function daysRemaining($dt){
        $future = strtotime($dt);
        $now = time();
        $timeleft = $future-$now;
        $daysleft = round((($timeleft/24)/60)/60);
        return $daysleft;
    }
}

if(!function_exists('generatePDF')){
    function generatePDF($path, $packageInfo, $customer){
        $mpdf = new \Mpdf\Mpdf();
            $html = '
                    <html>
                    <head>
                    <style>
                    body {font-family: sans-serif;
                        font-size: 10pt;
                    }
                    p {	margin: 0pt; }
                    table.items {
                        border: 0.1mm solid #000000;
                    }
                    td { vertical-align: top; }
                    .items td {
                        border-left: 0.1mm solid #000000;
                        border-right: 0.1mm solid #000000;
                    }
                    table thead td { background-color: #EEEEEE;
                        text-align: center;
                        border: 0.1mm solid #000000;
                        font-variant: small-caps;
                    }
                    .items td.blanktotal {
                        background-color: #EEEEEE;
                        border: 0.1mm solid #000000;
                        background-color: #FFFFFF;
                        border: 0mm none #000000;
                        border-top: 0.1mm solid #000000;
                        border-right: 0.1mm solid #000000;
                    }
                    .items td.totals {
                        text-align: right;
                        border: 0.1mm solid #000000;
                    }
                    .items td.cost {
                        text-align: "." center;
                    }
                    </style>
                    </head>
                    <body>
                    <h2>CLUB SC</h2>
                    <div style="text-align: right;">Date: '. date('d/m/Y') .'</div>
                    <table width="100%" style="font-family: serif;"><tr>
                    <td width="45%" style=""><span style="font-size: 7pt; color: #555555; font-family: sans;">FROM:</span><br /><br />345 Anotherstreet<br />Little Village<br />Their City<br />CB22 6SO</td>
                    <td width="10%">&nbsp;</td>
                    
                    </tr></table>
                    <br />
                    <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
                    <thead>
                    <tr>
                    <td width="10%">Sr. No.</td>
                    <td width="70%">Description</td>
                    <td width="20%">Amount</td>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- ITEMS HERE -->
                    <tr>
                    <td align="center">1</td>
                    <td align="center">'.$packageInfo->package.'</td>
                    <td align="right">'.$packageInfo->amount.'</td>
                    </tr>
    
                    <!-- END ITEMS HERE -->
                    <tr>
                    
                    <td class="totals" colspan="2">Subtotal:</td>
                    <td class="totals cost">'.$packageInfo->amount.'</td>
                    </tr>
    
                    </tbody>
                    </table>
                    
                    </body>
                    </html>
                    ';
            $mpdf->WriteHTML($html);
            $fileName = $customer->id_customer.'_'.date('ymd').'.pdf';
            $mpdf->Output($path);
    }
}








