<?php

if (!function_exists('hash_password')){
	function hash_password($str){
		return password_hash($str, PASSWORD_DEFAULT);
	}
}

if (!function_exists('encrypt')){
	function encrypt($str){
		return openssl_encrypt($str, CIPHERING, ENCRYPTION_KEY, ENCRYPTION_OPTION, ENCRYPTION_IV);
	}
}

if (!function_exists('decrypt')){
	function decrypt($str){
		return openssl_decrypt($str, CIPHERING, DECRYPTION_KEY, ENCRYPTION_OPTION, DECRYPTION_IV);
	}
}
