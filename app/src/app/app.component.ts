import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { CommonConstants } from './config/CommonConstants';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  customerData: any;
  name: any;
  version = '';
  constructor(
    private menu: MenuController,
    private _router: Router,
    private _ss: StorageService,
    private _platform: Platform,) {}

  ngOnInit(): void {
    this.initializeApp()
    // this.customerData = this._ss.getStorage(CommonConstants.CUSTOMER_DATA);
    // if(this.customerData !== null){
    //   this.name = this.customerData.name
    // }
  }

  closeDrawer(){
    // this.menu.enable(false, 'first');
    this.menu.close('first');
  }

  initializeApp(){
    this._platform.ready().then(()=>{
      this._router.navigateByUrl('splash');
    })
  }

  drawerMenuClicked(type){
    this.menu.close('first');
    if(type == 'notifications'){
      this._router.navigate(['notifications'])
    }else if(type == 'about'){
      this._router.navigate(['about-app'])
    }else if(type == 'coach'){

    }
  }

  logout(){
    this.menu.close('first');
    this._ss.clear();
    this._router.navigate(['/signin']);
  }
}
