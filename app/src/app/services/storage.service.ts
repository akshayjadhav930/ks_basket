import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  store(storageKey: string, value: any){
    const encryptedValue = btoa(escape(JSON.stringify(value)));
    localStorage.setItem(storageKey, encryptedValue);
  }

  getStorage(storageKey: string){
    const res = localStorage.getItem(storageKey);
    if(res){
      return JSON.parse(unescape(atob(res)));
    }else{
      return null;
    }
  }

  removeItem(storageKey: string){
    localStorage.removeItem(storageKey);
  }

  clear(){
    localStorage.clear();
  }
}
