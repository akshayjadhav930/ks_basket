import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(
    public _toastCtrl: ToastController,
    private _ss: StorageService) { }

  async presentToast(message: string, color: string) {
    const toast = await this._toastCtrl.create({
      message: message,
      cssClass: `toast-success`,
      color: color,
      duration: 1000,
      position: 'middle'
    });
    toast.present();
  }

  getIdealWeight(gender, height){
    let heightFeet = height * 0.0328084;
    let extraFeet = heightFeet > 5 ? heightFeet - 5 : 0;
    let extraInches = extraFeet * 12;
    
    let weight = gender == 'male' ? 52 + (extraInches * 1.9) : 49 + (extraInches * 1.9); 
    return weight.toFixed();
  }
  getIdealParams(gender, param){
    switch (param) {
      case 'visceral_fat': {
        if(gender == 'male'){
          return '1-8';
        }else{
          return '1-4';
        }
        break;
      }
      case 'tsf': {
        if(gender == 'male'){
          return '8-15';
        }else{
          return '8-15';
        }
        break;
      }
      case 'total_fat_per': {
        if(gender == 'male'){
          return 25;
        }else{
          return 15;
        }
        break;
      }
      case 'bmi': {
        return 23;
        break;
      }
      case 'muscles_per': {
        if(gender == 'male'){
          return 35;
        }else{
          return 27;
        }
        break;
      }
      default: {
        //statements; 
        break;
      }
    }
  }

  getReportRatio(gender, param, value) {
    switch (param) {
      case 'visceral_fat': {
        if(gender == 'male'){
          return value >=1 && value<=8 ? "green" : "red";
        }else{
          return value >=1 && value<=4 ? "green" : "red";
        }
        break;
      }
      case 'tsf': {
        if(gender == 'male'){
          return value >=8 && value<=15 ? "green" : "red";
        }else{
          return value >=8 && value<=15 ? "green" : "red";
        }
        break;
      }
      case 'total_fat_per': {
        if(gender == 'male'){
          return value <= 25 ? "green" : "red";
        }else{
          return value <= 15 ? "green" : "red";
        }
        break;
      }
      case 'bmi': {
        if(value <= 23){
          return "green";
        }else if(value > 23 && value <= 30){
          return "orange";
        }else{
          return "red";
        }
        break;
      }
      case 'muscles_per': {
        if(gender == 'male'){
          return value <= 35 ? "green" : "red";
        }else{
          return value <= 27 ? "green" : "red";
        }
        break;
      }
      default: {
        //statements; 
        break;
      }
    }

  }
}
