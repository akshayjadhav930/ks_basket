export class CommonConstants{
    //Local storage constants
    public static readonly IS_LOGIN = 'ks_basket_logged_in';
    public static readonly USERNAME = 'ks_basket_username';    
    public static readonly APP_VERSION = '1.0.0';    
}