import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonConstants } from '../config/CommonConstants';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private _ss : StorageService, private router : Router){}
  canActivate() : Promise<boolean>{
    return new Promise( resolve => {
      let isSession = this._ss.getStorage(CommonConstants.IS_LOGIN);
      if(isSession){
        this.router.navigate(['home']);
        resolve(false);
      }else{
        resolve(true);
      }
    });
  }
  
}
