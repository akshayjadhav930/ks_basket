import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'add-item',
    pathMatch: 'full'
  },
  {
    path: 'signin',
    canActivate: [LoginGuard],
    loadChildren: () => import('./pages/signin/signin.module').then( m => m.SigninPageModule)
  },
  {
    path: 'splash',
    loadChildren: () => import('./pages/splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'list-item',
    loadChildren: () => import('./pages/list-item/list-item.module').then( m => m.ListItemPageModule)
  },
  {
    path: 'add-item',
    loadChildren: () => import('./pages/add-item/add-item.module').then( m => m.AddItemPageModule)
  },
  {
    path: 'add-sale',
    loadChildren: () => import('./pages/add-sale/add-sale.module').then( m => m.AddSalePageModule)
  },
  {
    path: 'list-sale',
    loadChildren: () => import('./pages/list-sale/list-sale.module').then( m => m.ListSalePageModule)
  },
  {
    path: 'itemwise-sales',
    loadChildren: () => import('./pages/itemwise-sales/itemwise-sales.module').then( m => m.ItemwiseSalesPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
