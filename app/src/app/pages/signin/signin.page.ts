import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LoadingController, MenuController } from "@ionic/angular";
import { CommonConstants } from "src/app/config/CommonConstants";
import { CommonService } from "src/app/services/common.service";
import { HttpService } from "src/app/services/http.service";
import { LoaderService } from "src/app/services/loader.service";
import { StorageService } from "src/app/services/storage.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.page.html",
  styleUrls: ["./signin.page.scss"],
})
export class SigninPage implements OnInit {
  version = "";
  loginForm: FormGroup;
  constructor(
    private _fb: FormBuilder,
    private _http: HttpService,
    private _cs: CommonService,
    private _ss: StorageService,
    private _router: Router,
    private _loadingCtrl: LoadingController,
    private _menu: MenuController
  ) {
    this.loginForm = this._fb.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
  }

  ngOnInit() {
    this._menu.enable(false, 'first')
    this.version = CommonConstants.APP_VERSION;
  }

  get frm() {
    return this.loginForm.controls;
  }

  async submitForm() {
    this.loginForm.markAllAsTouched();
    if (!this.loginForm.valid) {
      return;
    } else {
      const loading = await this._loadingCtrl.create({
        cssClass: 'my-custom-class',
        message: 'Please wait...'
      });
      await loading.present();
      this._http.post_by_observable('checkLogin', this.loginForm.value).subscribe(res => {
        if (res.error == 0) {
          this._cs.presentToast(res.message, 'success');
          this._ss.store(CommonConstants.IS_LOGIN, 1);
          this._ss.store(CommonConstants.USERNAME, res.user);
          loading.dismiss();
          this._router.navigate(['home']);
        } else {
          loading.dismiss();
          this._cs.presentToast(res.message, 'danger');
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      })
    }
  }
}
