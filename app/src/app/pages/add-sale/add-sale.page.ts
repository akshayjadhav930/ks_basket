import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonModal, LoadingController, Platform } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { HttpService } from 'src/app/services/http.service';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { OverlayEventDetail } from '@ionic/core/components';
import { StorageService } from 'src/app/services/storage.service';
import { CommonConstants } from 'src/app/config/CommonConstants';

@Component({
  selector: 'app-add-sale',
  templateUrl: './add-sale.page.html',
  styleUrls: ['./add-sale.page.scss'],
})
export class AddSalePage implements OnInit {
  saleForm: FormGroup;
  itemList = [];
  saleItem = [];
  itemName = '';
  itemCost: any;
  itemCode = '';
  itemImg = '';
  id_item = 0;
  totalAmt = 0;
  isModalOpened = false;
  @ViewChild(IonModal) modal: IonModal;
  @ViewChild('qrmodal') qrmodal: IonModal;
  @ViewChild('inputId', {static: false}) ionInput: { setFocus: () => void; };
  constructor(
    private _fb: FormBuilder,
    private _http: HttpService,
    private _cs: CommonService,
    private _router: Router,
    private _loadingCtrl: LoadingController,
    private _androidPermissions: AndroidPermissions,
    private _platform: Platform,
    private _ss: StorageService
  ) {
    this.saleForm = this._fb.group({
      customer_name: ["", [Validators.required]],
      town: ["Kolhapur", [Validators.required]],
      payment_method: ["", [Validators.required]],
      whatsapp_no: ["", [Validators.required]],
      totalAmt: ["", [Validators.required]]
    });
  }

  ionViewWillEnter() {
    this.getItems();
  }

  ngOnInit() {
    this._platform.backButton.subscribeWithPriority(10, () => {
      if(this.isModalOpened){
        this.cancel();
      }else{
        this._router.navigate(['list-sale']);
      }
      
    })
  }

  cancel() {
    this.isModalOpened = false;
    this.modal.dismiss(null, 'cancel');
  }

  cancelQr(){
    this.qrmodal.dismiss(null, 'cancel');
  }

  confirmQr(){
    this.qrmodal.dismiss(null, 'cancel');
  }

  confirm() {
    if (this.itemCode !== '' && this.itemName !== '' && this.itemCost !== '') {
      const dt = {
        id_item: this.id_item,
        item_code: this.itemCode,
        item_name: this.itemName,
        item_cost: this.itemCost
      }
      this.totalAmt = this.totalAmt + parseInt(this.itemCost);
      this.frm.totalAmt.setValue(this.totalAmt);
      this.saleItem.push(dt);
      this.itemCode = '';
      this.itemName = '';
      this.itemCost = '';
      this.modal.dismiss('confirm');
      this.isModalOpened = false;
      console.log("this.saleItem", this.saleItem);
    } else {
      if (this.itemCode == '') {
        this._cs.presentToast("Please add item code", "danger")
      } else if (this.itemName == '') {
        this._cs.presentToast("Please select item name", "danger")
      } else if (this.itemCost == '') {
        this._cs.presentToast("Please add item cost", "danger")
      }
    }

  }


  deleteItem(id) {
    this.saleItem = this.saleItem.filter(sale => {
      if (sale.id_item != id) {
        return sale;
      }else{
        this.totalAmt = this.totalAmt - sale.item_cost;
        this.frm.totalAmt.setValue(this.totalAmt);
      }
    })
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    this.isModalOpened = false;
    if (ev.detail.role === 'confirm') {
      console.log("confirmed")
    }
  }

  get frm() {
    return this.saleForm.controls;
  }

  onItemSelected() {
    Object.keys(this.itemList).forEach(key => {
      if (this.itemList[key].item_name == this.itemName) {
        this.itemCode = this.itemList[key].item_code;
        this.itemCost = parseInt(this.itemList[key].item_cost);
        this.id_item = this.itemList[key].id_item;
        this.itemImg = this.itemList[key].item_img;
        this.ionInput.setFocus();
      }
    });
  }

  onPaymentTypeChanged(e){
    const val = e.target.value;
    if(val == 'upi'){
      this.qrmodal.present();
    }
  }

  onItemCodeChanged(e) {
    let item_code = e.target.value;
    let found = false;
    Object.keys(this.itemList).forEach(key => {
      if (this.itemList[key].item_code.toLowerCase() == item_code.toLowerCase()) {
        this.itemName = this.itemList[key].item_name;
        this.itemCost = parseInt(this.itemList[key].item_cost)
        this.id_item = this.itemList[key].id_item;
        this.itemImg = this.itemList[key].item_img;
        found = true;
        this.ionInput.setFocus();
      }
    });
    if (!found) {
      this.itemName = '';
      this.itemCost = '';
      this.id_item = 0;
      this.itemImg = '';
    }
  }

  addItem() {
    this.itemCode = '';
    this.itemName = '';
    this.itemCost = '';
    this.id_item = 0;
    this.itemImg = '';
    this.isModalOpened = true;
  }

  async submitForm() {
    if (this.saleForm.invalid) {
      this._cs.presentToast('Please add all the details', 'danger');
    } else {
      if (this.saleItem.length > 0) {
        const loading = await this._loadingCtrl.create({
          cssClass: 'my-custom-class',
          message: 'Please wait...'
        });
        await loading.present();
        const params = {
          ...this.saleForm.value,
          sale_items: this.saleItem,
          user: this._ss.getStorage(CommonConstants.USERNAME)
        }
        this._http.post_by_observable('addSale', params).subscribe(res => {
          loading.dismiss();
          if (res.error == 0) {
            this._cs.presentToast(res.message, 'success');
            this._router.navigate(['list-sale']);
          } else {
            this._cs.presentToast(res.message, 'danger');
          }
        }, err => {
          loading.dismiss();
          this._cs.presentToast('Something went wrong! Please try later', 'danger');
          console.log("error sale", err);
        })
      } else {
        this._cs.presentToast("Please add atleast one item", "danger")
      }

    }
  }

  async getItems() {
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    this._http.get_by_observable('getItems').subscribe(res => {
      loading.dismiss();
      if (res.error == 0) {
        this.itemList = res.data;
        this.itemList = this.itemList.filter(item => item.is_sold == 0)
        console.log("this.itemList", this.itemList)
      } else {
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }

}
