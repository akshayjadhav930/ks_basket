import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { HttpService } from 'src/app/services/http.service';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { StorageService } from 'src/app/services/storage.service';
import { CommonConstants } from 'src/app/config/CommonConstants';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.page.html',
  styleUrls: ['./add-item.page.scss'],
})
export class AddItemPage implements OnInit {
  itemForm: FormGroup;
  imgBase64 = '';
  constructor(
    private _fb: FormBuilder,
    private _http: HttpService,
    private _cs: CommonService,
    private _router: Router,
    private _loadingCtrl: LoadingController,
    private _androidPermissions: AndroidPermissions,
    private _platform: Platform,
    private _ss: StorageService
  ) {
    this.itemForm = this._fb.group({
      item_name: ["", [Validators.required]],
      item_img: ["", [Validators.required]],
      item_code: ["", [Validators.required]],
      item_cost: ["", [Validators.required]],
      item_desc: [""],
    });
  }

  ngOnInit() {
    this._platform.backButton.subscribeWithPriority(10, () => {
      this._router.navigate(['list-item']);
    })
    this._androidPermissions.checkPermission(this._androidPermissions.PERMISSION.CAMERA).then(
      result => {
        if (!result.hasPermission) {
          this._androidPermissions.requestPermission(this._androidPermissions.PERMISSION.CAMERA)
        } else {
          console.log("permission granted for camera")
        }
      },
      err => this._androidPermissions.requestPermission(this._androidPermissions.PERMISSION.CAMERA)
    );
  }

  async capturePhoto() {
    const image = await Camera.getPhoto({
      quality: 30,
      width: 300,
      height: 300,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera
    });
    this.imgBase64 = image.base64String;
    // this.imgBase64 = '/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAIQAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAGxIUFxQRGxcWFx4cGyAoQisoJSUoUTo9MEJgVWVkX1VdW2p4mYFqcZBzW12FtYaQnqOrratngLzJuqbHmairpP/bAEMBHB4eKCMoTisrTqRuXW6kpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpKSkpP/AABEIAOEBLAMBIgACEQEDEQH/xAAaAAADAQEBAQAAAAAAAAAAAAAAAQIDBAUG/8QANRAAAgIBAwMCBAUDAwUBAAAAAAECESEDMUESUWFxgQQikaETMkKxwdHh8CMz8QU0UmKSgv/EABcBAQEBAQAAAAAAAAAAAAAAAAABAgP/xAAbEQEBAQADAQEAAAAAAAAAAAAAARECITESIv/aAAwDAQACEQMRAD8AzSynz4BrP8LkOWu/IN5ztxZzUen3Bfm9ylzyJrzVEUk21d/cajYnt2Gr54AKezfrQkqz2G208c9gt8bGoD9ya3Q+R11YIjFxcGnH7Gmm7bu/JXjZkOObTposqunS1UpKt9ju0dW0ePBXLhS7G+hruDS45Nys2PXaUl3X7EV04e/D7k6WrfJs0pKt0aZZslop3DDzHuFWBnJHJ8R8KtS5Q+Wfc7WsMiSyB5sZ6mhJLUi2u9bnbDVhqZTsvpUlTRzz+DSdwbg/HIR0zjGWnJPZo8T4jTcNRqj0Pw/iIbT6l2Zhr6WpKNySwSxeNx5737HR8FqKHU5VuYTSTpWh306GMNsjd7d/xGqnF5OPW+Ix0wuq3MbUqbbsLtt16MM42hLqjl/Mge+SIRk2qx6mvTLN8mW5UNXTVexSppusLfwKs+V2Fs8Nr0Bh3+iXsxpfo1F4JWL5S+w3HqXftZUTT6qr+5WrDodtbrK7hpunT42K1pOcurllGS/Tb22yUums033FSd8MhNrDphHpN3fjOBJV7ZHlrPA3jY5tDbNBSq63GljG5NZfTsA83W5PLp36l+GxJICeyCsd8lPYTpLGAJ323HfANf4g9QDjORK14HTed1uCXfcITipevdGbbi1b9zYlrqTTLBr8NrdDy89j0dHWUlg8fpqSV12Z1aUsYdSRuJXqOmjNxazH6EaGt1Rzjg1rk0yztO635Qnsy5RUnnD7oh3HEvr3Al4Y0FCW4Q2sbZFKK6boNS5V0ukYaur0ppSySjzP+oaK09W1+ow16SjDhKzs1EpvLs4tVuWrJv6GW4iEOp1dPe2dENJR8+pnDTcdNSfJrDTk49s7lKH1Xea4K6JVdb8lJK8NGicW6dUiYaxay7ZkqcvP2OnWUYJyXBg6mrivUz41LqVaeMfyVF7Yx+xLd9wqn1JpUVRO33b7oFOk+t4RSpptbfdESV3L+SsnNdMlW3AOUcdduXuTGWak6X2KppYbRR2y3tUUvA7UntTXgKXt6nNSrv6Dt4rYHiqdtjpf3AXZrcHtYmseQ28oB/YX9R0mr+4c5AlpX2D1GFAGCWnxgYAAxcZABPIot6btPHJQmiy4OnR1oudd+Tu09RSWDx0umVpbcHRpa7pu8I6Ss2PR3G0mqatMw09dSWWa6c1NNorKelxyvmXblEOSdtFzmoryZOcNR1K4v/yQGWrq8R+pzyNtTSlDNJxezWxhN+5lWMu3Bi9K5J02uTZ5eTf4bSUmk5VfINYdLUU5RT8BKVqkqZ06sV0qqoy/B+W2xgyjCSj1cFJuUsoUpv8AE6FaVexeulFR6ccbE5XBGrFSiksKsmTj0qorJslKNOWY8i1IRmv9NrP0M7txWH51cXj9iXl/0F/qaU6klk06eqnHPfwWzG5dQsW099/I208q2vXKE88CjLparbs+QYUlef1c0EW0qk6+43v1Kq7D65PZfcrL0MU1SHdPIXsCeHsYU68EtJPuPnkE63z7AK0GbxljewVVATwDb5GxZSAEMW67UN4wwCgewJWAE4+gP0spITAQfsNie5QnyS+aG/IgKhPK49zq+Hm75o4ZK32N9DVa3z2Nys2OuTt5IeDOGq+r5hz1OEEUtZ6dpO1ynsRPT09e3pYlzB/wZy7kXVNPIEyTTqSpl6Wp077GqnHVio62JcTRnqaL03Utns+4Gk3CafTJVuRCVPpd0YVnwTK2t2XRpOlPqbVLahtua227nMpdMk55R1aMbhcliW+SX5s2ha0lKPSl9HgwXX0fLTS7m2YWrVMw1dSkoql/Jz6l/KiWoop7O1kxjqdEr4eCHLInb5o3xm+q6Zx5V1xRDXtgWlJpJSeO5coJO07RLMaZ3WUl5LSxauvCsVNLjGSajyn/APQSvSzdcjTw2JSxVeg8XfcyD/MBlcgu1iv6EBWOAu7D3D6lAwB1YbboAarsAWDWMAL0Dqyxu6E9igB9wFYDJryO/IWFJiZSVvBahyyxLWUYN7mnTSwXQqKymh+BtCYCe1kRy2zTBnF9LplQ3hGmnq1Homr0+3b0IewgHraPQuqPzQezRjJG2nrfhtprqg94hraS6fxNL5oPHoByyiidTWkoRhFZXJpK1hmckvYlVk5ye7bbJ2/5Bqnu7BO2ZaTFdTS3RepGKXytt3lD01c74Reg4/iSbaqryalxGCk1g30m3p22mitKSl1daVJdjmlNp2lSstso6HFxtqmiKvNv2VmmnPqirE7jiMsb7mGnc684CSxaQVS29LNNOCaUpLHFEQo6bfzWkTJOL2SZpqaiWIpWKEHJ3Igzv2DNujTVlBYSyuTO2UK/qxitPcN+QH6BYsjSbdcgAhyi1yTuigCg2BMKKyOK6uAjFtmyVbGmbSjBIY0AROwigoCRNYLaonyBLyiXFSWSxUUZrT/9mP8ADXLbLAIjpXYcJvTbrMXunyMT3AWvoLoc9N3B/Y5krjk6oTlDK913DV0ouPXpr5eV2IOCcenGSKazZ0yin/mxn+H+qWEuO5Ma1P8At6bvHVkySblebNJyc5NvCJw+2SaQqwuRKDlJRUcsqWItmPW7+VuyyDoinpzcJNJ9jWMl0qlZxRUnK+X3NozaWfsWw162rNSvpSb7lwUn+bkIaVPOfBOpqSeIp/1OSofTGVXdcm3zShio2Tp6dZe7NL8AY/hPuL8Jo2YvuXRl+G+KE9N1g3ZlKeaWR2IUc08GjahhK2RSWZZGrk72KDpSzNkylfog1U07bJpywiyATKjDuVCHTmi0jTOhIfqFAAIHwMOAEh1kAAVCooRUTQmUDRBFCLaJrwUJIJZyPgQESje44ScHcV6l1fkmvAE6mlFpzgk48rsY6ickdC6ovqSXldyZwVdcMx7diDimmuMGbu97OqUb2OeSakZalRN/J4RjS+p0PaqM5w+W+3c1Bmnku5UZ5uqKT8/Uo+hvG6TH4snZZT9AvD/Y4qbumF7YJvAs7gX1K0mT5sm/sH1KirYqQkwb/uFJxTdtDuhOSSyJJy3wuxcTSlc3jYqMaWEVVDqjSFRSFQyoKQUHqAAFBeA4CgAHwEIA8gACYxFC9RfsUxUBNAUJq+CBc+B8BWQW4EPwSm4O1/yaNZ2E13CM5aamuvTWeV2OecOrng6cxdxdMc1DV3ahLvwRdec07oaS23Zvr6EoPMfR9zBmWilpxrC+pFV/ZGnPngzdv/g1LR66cth9TraiOpp70jHW+IccRMyK11NZQzJ57GWlqy1J3tHajDTjLVn8z+XydaUYqlsW9C+rnYV92S3QnJLkgu8i6u2WZqTlt9S4pLZ5LIzaqKd23ZovBC9SsGkUAk7G7AaEgCgGAcDAXIw5AABhQAGzAKDYoQAwQAJoqhMBcBVjSEgFQNlV3JeF/JAhVfA2J5YQqIlE0YpbAR+JOFpO12eUQ4aGtj/bm/8A5KlH6mMvld5ruSrE63wsoLqjG+0lsZ9K7tfQrS1dWE30zddq3N3raUsz0IOXe2g0H8F8W473i9zGfwXxEE24Y7Wex1diJNu0ue5pnXnaf4kYVSpdgnrSi63fZHXqQUdNuODHS06tu22Sw1g9Sb2iHTqajTTTS3R1S0rV00iXDpytgJglRaslrpl2TzZarkB8DTxsCzkpYoAVjAaSAXgfuCXcdAIB0OgFQDpggEgfkbE07wA6AKAoQDoAEHgboVAFeRcAACDLHkVUiCV3BqlaKoTVlRLBWOvANNeezIqJJVxZnKPUrrJtVerFS52YRxTjUuy9ASbWML0OjV0+pLujnp9mGo9X/Koa9PcdOwpYwGUTTcWtiPgknP53Rs1jycknLTbyvBR6GtC0cso/YvS16jHr5WPAasf1IDGrTTtERXD3Rqk01ROoumfVnJAcDWUTuikFNO+RiW43vsAxiTAopDF4oaIATQw9ShcByMEiAW2RMYigEVwIBegnsULgBbITvllUD7MgleP2HQUt+QfDQCFV/WyvbAbWBNciksF8PuKs7BENfLSdpk3wvsatL/GJrmwM3hGepCEpXJK/Q1ks8EqkgrszW4dOBurtAVClSVnJrRqbl3OzdHN8TGqYFKX4sY8Z4OqKj0s5fhumvzU07NpSaacQJmumV7Iia64Orvg6ZRU47JnLOLg8kVnHP/Ba8EP5NRXsxq8AVvgd92L3BbVQFfuMnkoAKJGAwBbAAeRgIofAgGAhMoTASFvuMHlECQch6DKEtxUOmNeCCWrBWVQejAlKmMdcsV4AVeAaXeh1juKm9ueAiJR5axyQ0rzF/Q0ecduBxinba55kFbryG7Hb7EvNsqGtjH4iK6N80bRV+ETrxuL9AOOMnGSd87HVKSUKOOWYtNYN9NrpVvb5WRQnqQf4kJNrsbQcPiY3dNbxZEpU/ldUtjKTcZLWhjugNNTTqNERtLJvp68NaPkynUdRwyAR9BpCXGRugG8BdBQwgruMECCmCENFQA8DrIuAGAkPwRQgAChMTQwogkr1CsAkAceQoPcfGAFWwbjS7MdcICWvoJ/UpLCEwhXjvwJLuHGw2BErbwKSd8e5TtrbCJ919QOh3doiSfsXYmihQdLJTVp+gk6Q/oBwzxJri8+RwcYvKxY/iFWpslyYu67/AMkV6HSum7WTGunUcWl0y/cehqdWir3TrAanzLGKA5VOej8R01h7HTq1LTjNXfK7GWtG1DVvPVk01n0Rru8ChrZPHrY8smLtFrKwQC8jxwCXuOioF9g4BbD4AAQMNih8BWAHwBPNBX1KaFuAUAw8hQLkpC9iBDodAUTzyMddxPwyAXfYBpCAOEJqg2Qc7ATViu+xUqJaCJdiT7KT9EUhxSr+wFS291/JcuAApE/qfp/JcN/qABXH8X/uL0Ofn2/lgBkbfDbe/wDU2lwAARqf9u/UPidogBQ1t7lLgAAuG0Rx/lgAD5Jf8AAD4iN7ewAEH6v/AMjewAUVx7iX8ABFC2D+gAUD5BbgBAnuin+VgAAt2KX5QAAlsg/UAATLZ+gnx6gACX5l7A9gAgl/mfp/QOAAD//Z';
    this.imgBase64 = 'data:image/png;base64,'+this.imgBase64;
    this.frm.item_img.setValue(this.imgBase64);
  }

  get frm() {
    return this.itemForm.controls;
  }

  async submitForm() {
    if(this.itemForm.invalid){
      this._cs.presentToast('Please add all the details', 'danger');
    }else{
      const loading = await this._loadingCtrl.create({
        cssClass: 'my-custom-class',
        message: 'Please wait...'
      });
      await loading.present();
      const params = {
        ...this.itemForm.value,
        user: this._ss.getStorage(CommonConstants.USERNAME)
      }
      this._http.post_by_observable('addItem', params).subscribe(res => {
        loading.dismiss();
        if(res.error == 0){
          this._cs.presentToast(res.message, 'success');
          this._router.navigate(['list-item']);
        }else{
          this._cs.presentToast(res.message, 'danger');
        }
      }, err =>{
        loading.dismiss();
        this._cs.presentToast('Something went wrong! Please try later', 'danger');
        console.log("error item", err);
      })
    }
  }
}
