import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { CommonConstants } from 'src/app/config/CommonConstants';
import { CommonService } from 'src/app/services/common.service';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-list-sale',
  templateUrl: './list-sale.page.html',
  styleUrls: ['./list-sale.page.scss'],
})
export class ListSalePage implements OnInit {
  searchTerm = '';
  sales: any;
  tmpSales: any;
  constructor(
    private _router: Router,
    private _http: HttpService,
    private _cs: CommonService,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    private _platform: Platform,
    private _ss: StorageService
  ) { }

  ngOnInit() {
    this._platform.backButton.subscribeWithPriority(10, () => {
      this._router.navigate(['home']);
    })
  }

  ionViewWillEnter(){
    this.getSales();
  }

  search(){
    if(this.searchTerm !== ''){
      this.tmpSales = this.sales.filter(c => {
        if(c.customer_name.includes(this.searchTerm) || c.town == this.searchTerm){
          return c;
        }
      })
    }else{
      this.tmpSales = this.sales;
      this._cs.presentToast("Please enter search term!", "danger")
    }
  }

  async presentConfirmAlert(id) {
    const alert = await this._alertCtrl.create({
      header: 'You want to delete?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // this.deleteItem();
          },
        },
        {
          text: 'Confirm',
          role: 'confirm',
          handler: () => {
            this.deleteSale(id);
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  clearSearch(){
    this.tmpSales = this.sales;
  }

  addSale(){
    this._router.navigate(['add-sale'])
  }

  async getSales(){
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    this._http.get_by_observable('getSales').subscribe(res => {
      loading.dismiss();
      if(res.error == 0){
        this.sales = res.data;
        this.sales = this.sales.map(sale => {
          sale.isExpanded = false;
          return sale;
        })
        this.tmpSales = this.sales;
      }else{
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }

  toggle(id){
    
    this.tmpSales = this.tmpSales.map(sale => {
      if(sale.id_sale == id){
        sale.isExpanded = !sale.isExpanded;
      }else{
        sale.isExpanded = false;
      }
      return sale;
    });
    console.log("this.tmpSales", this.tmpSales)
  }

  async deleteSale(id){
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    const params = { id: id, user: this._ss.getStorage(CommonConstants.USERNAME) };
    this._http.post_by_observable('deleteSale', params).subscribe(res => {
      loading.dismiss();
      if(res.error == 0){
        this.getSales();
        this._cs.presentToast(res.message, 'success')
      }else{
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }
}
