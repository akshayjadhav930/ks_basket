import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ItemwiseSalesPageRoutingModule } from './itemwise-sales-routing.module';

import { ItemwiseSalesPage } from './itemwise-sales.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ItemwiseSalesPageRoutingModule
  ],
  declarations: [ItemwiseSalesPage]
})
export class ItemwiseSalesPageModule {}
