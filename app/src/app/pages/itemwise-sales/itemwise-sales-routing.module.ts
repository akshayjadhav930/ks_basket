import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemwiseSalesPage } from './itemwise-sales.page';

const routes: Routes = [
  {
    path: '',
    component: ItemwiseSalesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItemwiseSalesPageRoutingModule {}
