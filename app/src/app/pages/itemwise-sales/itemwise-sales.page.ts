import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { HttpService } from 'src/app/services/http.service';
@Component({
  selector: 'app-itemwise-sales',
  templateUrl: './itemwise-sales.page.html',
  styleUrls: ['./itemwise-sales.page.scss'],
})
export class ItemwiseSalesPage implements OnInit {
  searchTerm = '';
  items: any;
  tmpItems: any;
  constructor(
    private _router: Router,
    private _http: HttpService,
    private _cs: CommonService,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.getItemwiseSales();
  }

  search(){
    if(this.searchTerm !== ''){
      this.tmpItems = this.items.filter(c => {
        if(c.item_name.includes(this.searchTerm) || c.item_code == this.searchTerm){
          return c;
        }
      })
    }else{
      this.tmpItems = this.items;
      this._cs.presentToast("Please enter search term!", "danger")
    }
  }

  clearSearch(){
    this.tmpItems = this.items;
  }

  addItem(){
    this._router.navigate(['add-item'])
  }

  async getItemwiseSales(){
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    this._http.get_by_observable('getItemwiseSale').subscribe(res => {
      loading.dismiss();
      if(res.error == 0){
        this.items = res.data;
        this.tmpItems = res.data;
      }else{
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }
}
