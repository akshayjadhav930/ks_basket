import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss']
})
export class SplashPage implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
    // this._router.navigate
  }

  ionViewWillEnter(){
    setTimeout(function(){
      this._router.navigate(['home']);
    }.bind(this),2000)
  }
}
