import { Component, OnInit } from "@angular/core";
import { AlertController, LoadingController, MenuController, Platform } from "@ionic/angular";
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { CommonConstants } from "src/app/config/CommonConstants";
import { StorageService } from "src/app/services/storage.service";
import { HttpService } from "src/app/services/http.service";
import { CommonService } from "src/app/services/common.service";
import { Router } from "@angular/router";
import { Location } from '@angular/common';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  totalItems = 0;
  totalSales = 0;
  totalCustomers = 0;
  constructor(
    private menu: MenuController,
    private _ss: StorageService,
    private _http: HttpService,
    public _cs: CommonService,
    private _router: Router,
    private _loadingCtrl: LoadingController,
    private _alertCtrl: AlertController,
    private _platform: Platform,
    private _location: Location
  ) { }

  ionViewWillEnter() {
    this.getDashboardData();
  }
 
  ngOnInit() {
    this._platform.ready().then(() => {
      this._platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
        console.log('Back press handler!');
        if (this._location.isCurrentPathEqualTo('/home')) {
          // Show Exit Alert!
          console.log('Show Exit Alert!');
          this.showExitConfirm();
          processNextHandler();
        } else {
          processNextHandler();
          // Navigate to back page
          console.log('Navigate to back page');
          this._location.back();
        }
      });
  
      this._platform.backButton.subscribeWithPriority(5, () => {
        console.log('Handler called to force close!');
        this._alertCtrl.getTop().then(r => {
          if (r) {
            navigator['app'].exitApp();
          }
        }).catch(e => {
          console.log(e);
        })
      });
    });
  }

  gotoItems(){
    this._router.navigate(['list-item']);
  }

  gotoSales(){
    this._router.navigate(['list-sale']);
  }

  gotoItemwiseSales(){
    this._router.navigate(['itemwise-sales']);
  }

  async getDashboardData() {
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });
    await loading.present();
    this._http.get_by_observable('getDashboard').subscribe((res) => {
      loading.dismiss();
      if (res.error == 0){
        this.totalItems = res.total_items;
        this.totalSales = parseInt(res.total_sale);
        this.totalCustomers = res.total_customers;
      } else {
        this._cs.presentToast(res.message, 'danger');
      }
    }, (err) => {
      loading.dismiss();
      console.log(err);
    })
  }

  showExitConfirm() {
    this._alertCtrl.create({
      header: 'Inspection termination',
      message: 'Do you want to exit Assigning?',
      backdropDismiss: false,
      buttons: [{
        text: 'Stay',
        role: 'cancel',
        handler: () => {
          console.log('Application exit prevented!');
        }
      }, {
        text: 'Exit',
        handler: () => {
          navigator['app'].exitApp();
        }
      }]
    })
      .then(alert => {
        alert.present();
      });
  }

  logout(){
    this._ss.clear();
    this._router.navigate(['signin'])
  }

}
