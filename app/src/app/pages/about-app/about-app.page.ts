import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
@Component({
  selector: 'app-about-app',
  templateUrl: './about-app.page.html',
  styleUrls: ['./about-app.page.scss'],
})
export class AboutAppPage implements OnInit {
  version: any;
  constructor(private _appVersion: AppVersion) { }

  ngOnInit() {
    this._appVersion.getVersionCode().then(res => {
      this.version = res;
    })
  }

}
