import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { CommonConstants } from 'src/app/config/CommonConstants';
import { CommonService } from 'src/app/services/common.service';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.page.html',
  styleUrls: ['./list-item.page.scss'],
})
export class ListItemPage implements OnInit {
  searchTerm = '';
  items: any;
  tmpItems: any;
  constructor(
    private _router: Router,
    private _http: HttpService,
    private _cs: CommonService,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    private _platform: Platform,
    private _ss: StorageService
  ) { }

  ngOnInit() {
    this._platform.ready().then(() => {
      this._platform.backButton.subscribeWithPriority(10, () => {
        this._router.navigate(['home']);
      });
    })
    
  }

  ionViewWillEnter(){
    this.getItems();
  }

  search(){
    if(this.searchTerm !== ''){
      this.tmpItems = this.items.filter(c => {
        if(c.item_name.includes(this.searchTerm) || c.item_code == this.searchTerm){
          return c;
        }
      })
    }else{
      this.tmpItems = this.items;
      this._cs.presentToast("Please enter search term!", "danger")
    }
  }

  async presentConfirmAlert(id) {
    const alert = await this._alertCtrl.create({
      header: 'You want to delete?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // this.deleteItem();
          },
        },
        {
          text: 'Confirm',
          role: 'confirm',
          handler: () => {
            this.deleteItem(id);
          },
        },
      ],
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  clearSearch(){
    this.tmpItems = this.items;
  }

  addItem(){
    this._router.navigate(['add-item'])
  }

  async getItems(){
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    this._http.get_by_observable('getItems').subscribe(res => {
      loading.dismiss();
      if(res.error == 0){
        this.items = res.data;
        this.tmpItems = res.data;
      }else{
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }

  async deleteItem(id){
    const loading = await this._loadingCtrl.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await loading.present();
    const params = { id: id, user: this._ss.getStorage(CommonConstants.USERNAME) };
    this._http.post_by_observable('deleteItem', params).subscribe(res => {
      loading.dismiss();
      if(res.error == 0){
        this.getItems();
        this._cs.presentToast(res.message, 'success')
      }else{
        this._cs.presentToast(res.message, 'danger')
      }
    }, err => {
      loading.dismiss();
      this._cs.presentToast('Something went wrong. Please try later', 'danger')
      console.log('res err', err)
    })
  }

}
